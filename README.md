PUMA OpenCms Module
===================

This repository contains the source code of the BibSonomy official PUMA modules for OpenCms. These modules integrate functionality of the PUMA system in an OpenCms installation, allowing content editors to provide consistent views of publication lists.

OpenCms Modules
---------------
The funtionality has been separated into two OpenCms modules to improve the code maintainability:

* `de.academicpuma.opencms.core`: provides the libraries required to query PUMA
* `de.academicpuma.opencms.view`: contains the OpenCms formatters that render the results  


Building the modules
--------------------

This repository is structured in 5 maven projects that allow the building and assembly of the two opencms modules:

* `root`: super-project that allows to build all projects and modules
* `parent`: contains the configuration common to all maven projects
* `assembly`: maven instructions to build the OpenCms zip files
* `de.academicpuma.opencms.core`: OpenCms core module
* `de.academicpuma.opencms.view`: OpenCms view module

To build all modules:

    # CD into the repository root and...
    mvn clean package
    
Afterwards, you should be able to install both `de.academicpuma.opencms.*/target/de.academicpuma.opencms.*-0.0.999-opencms-module.zip` modules in OpenCms.