package de.academicpuma.opencms.core;

/**
 * all supported default styles
 * @author dzo
 */
public enum CSLStyles {
	
	/** APA style */
	APA("APA - American Psychological Association 6th edition", "apa"),
	
	/** Chicago */
	CHICAGO("Chicago Manual of Style 16th edition", "chicago-author-date-de"),
	
	/** DIN 1505-2 */
	DIN1505("DIN 1505-2","din-1505-2"),
	
	/** Harvard */
	HARVARD("Harvard - Institut fuer Praxisforschung", "harvard-institut-fur-praxisforschung-de"),
	
	/** the ieee style */
	IEEE("IEEE", "ieee"),
	
	/** Springer LNCS */
	LNCS("Springer LNCS - Lecture Notes in Computer Science", "springer-lecture-notes-in-computer-science"),
	
	/** the vancouver style */
	VANCOUVER("Vancouver", "vancouver");

	private final String name;
	
	/** the file name (without extension) */
	private final String filename;
	
	/**
	 * @param name
	 * @param filename
	 */
	CSLStyles(String name, String filename) {
		this.name = name;
		this.filename = filename;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return the filename
	 */
	public String getFilename() {
		return this.filename;
	}
}
