package de.academicpuma.opencms.core;

import de.academicpuma.opencms.core.xml.PumaPublicationList;

import org.apache.commons.logging.Log;
import org.bibsonomy.common.enums.GroupingEntity;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsLog;
import org.opencms.xml.content.CmsDefaultXmlContentHandler;
import org.opencms.xml.content.CmsXmlContentErrorHandler;
import org.opencms.xml.types.I_CmsXmlContentValue;

import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class CmsXmlPumaPublicationListHandler extends CmsDefaultXmlContentHandler {
	private static final Log LOG = CmsLog.getLog(CmsXmlPumaPublicationListHandler.class);

	public CmsXmlPumaPublicationListHandler() {
		LOG.debug("New instance of CmsXmlPumaPublicationListHandler created");
	}

	public CmsXmlContentErrorHandler resolveValidation(CmsObject cms, I_CmsXmlContentValue value, CmsXmlContentErrorHandler errorHandler) {
		LOG.debug("resolveValidation invoked on " + value.getName() + " (" + value.getStringValue(cms) + ")");
		logCurrentErrorsFromHandler("Errors saved previously: ", errorHandler);

		try {
			switch (value.getName()) {
				case PumaPublicationList.ELEMENT_API_USERNAME:
					JspCSLRenderBean.BeanParameters.parseApiUsername(value.getStringValue(cms));
					break;
				case PumaPublicationList.ELEMENT_API_KEY:
					JspCSLRenderBean.BeanParameters.parseApiKey(value.getStringValue(cms));
					break;
				case PumaPublicationList.ELEMENT_NUMBER_OF_PUBLICATIONS:
					JspCSLRenderBean.BeanParameters.parseNumberOfPublications(value.getStringValue(cms));
					break;
				case PumaPublicationList.ELEMENT_SOURCE_ID:
					validateSourceID(cms, value);
					break;
				default:
					break;
			}
		} catch (PumaException e) {
			if (errorHandler == null) {
				errorHandler = new CmsXmlContentErrorHandler();
			}
			errorHandler.addError(value, e.getLocalizedMessage(cms));
		}

		logCurrentErrorsFromHandler("Errors after resolveValidation: ", errorHandler);

		return errorHandler;
	}

	private static void logCurrentErrorsFromHandler(String logIntro, CmsXmlContentErrorHandler errorHandler) {
		if (LOG.isDebugEnabled()) {
			if (errorHandler != null) {
				Map<Locale, Map<String, String>> allErrors = errorHandler.getErrors();
				if (allErrors != null) {
					final String collectedErrors = allErrors.entrySet().stream().map(
							localeErrorsMapEntry -> {
								final Locale locale = localeErrorsMapEntry.getKey();
								final Map<String, String> localizedErrors = localeErrorsMapEntry.getValue();
								final String collectedLocalizedErrors = localizedErrors.entrySet().stream().map(
										// path -> message
										errorMapEntry ->
												errorMapEntry.getKey() + "=\"" + errorMapEntry.getValue() + "\""
								).collect(Collectors.joining(", ", "{", "}"));

								return "locale=" + locale + ":" + collectedLocalizedErrors;
							}
							).collect(Collectors.joining(", ", "{", "}"));
					LOG.debug(logIntro + collectedErrors);
				} else {
					LOG.debug("XXX:" + logIntro + " - errorHandler != null, but allErrors is null (!?)");
				}
			}
		}
	}

	/** Validate {@code Sourced-ID}, that depends on {@code Source}: if {@code Source} is {@code user}
	 *  or {@code group}, {@code Source-ID} cannot be empty. */
	private static void validateSourceID(CmsObject cms, I_CmsXmlContentValue value) throws PumaException {
		final String sourceID = value.getStringValue(cms);
		GroupingEntity source = null;
		if (value.getDocument() != null) {
			final I_CmsXmlContentValue sourceContentValue = value.getDocument().getValue(
					PumaPublicationList.ELEMENT_SOURCE, value.getLocale());
			if (sourceContentValue != null) {
				final String sourceString = sourceContentValue.getStringValue(cms);
				source = JspCSLRenderBean.BeanParameters.parseSource(sourceString);
			}
		}
		JspCSLRenderBean.BeanParameters.validateSourceAndSourceID(source, sourceID);
	}
}
