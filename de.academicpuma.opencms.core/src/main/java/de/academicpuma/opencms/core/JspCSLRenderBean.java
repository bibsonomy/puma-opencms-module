package de.academicpuma.opencms.core;

import de.academicpuma.opencms.core.util.CSLUtils;
import de.academicpuma.opencms.core.util.grouper.AuthorGrouper;
import de.academicpuma.opencms.core.util.grouper.EntrytypeGrouper;
import de.academicpuma.opencms.core.util.grouper.PublicationGrouper;
import de.academicpuma.opencms.core.util.grouper.TitleGrouper;
import de.academicpuma.opencms.core.util.grouper.YearGrouper;
import de.academicpuma.opencms.core.widget.InstituteSelectWidget;
import de.academicpuma.opencms.core.widget.SourceSelectWidget;
import de.academicpuma.opencms.core.xml.PumaBibTeXSelect;
import de.academicpuma.opencms.core.xml.PumaPublicationList;
import de.undercouch.citeproc.csl.CSLItemData;
import de.undercouch.citeproc.output.Bibliography;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.SortOrder;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.comparators.BibTexPostComparator;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.logic.LogicInterfaceFactory;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.opencms.jsp.CmsJspActionElement;
import org.opencms.jsp.util.CmsJspContentAccessValueWrapper;
import org.opencms.main.CmsException;
import org.opencms.main.CmsLog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.bibsonomy.util.ValidationUtils.present;

/**
 * helper bean to render csl
 *
 * @author dzo
 * @author ag
 */
// TODO: AG 2019-02-15 - Add @JavaBean (Java 9)
public class JspCSLRenderBean extends CmsJspActionElement {
	private static final Log LOG = CmsLog.getLog(JspCSLRenderBean.class);
	private static final Log CONTENT_LOG = CmsLog.getLog("CONTENT." + JspCSLRenderBean.class.getName());

	private static final String HTML_POSTS_GROUP_ENDING = "</ol></li>";

	/** @deprecated Currently not included in the schema */
	private static final String ELEMENT_YEAR_GROUPING = "Year-Grouping";

	private static final String TAG_SPLIT_REGEX = " ";

	private static final Map<SortKey, PublicationGrouper> GROUPER = new HashMap<>();

	static {
		GROUPER.put(SortKey.NONE, null);
		GROUPER.put(SortKey.AUTHOR, new AuthorGrouper());
		GROUPER.put(SortKey.ENTRYTYPE, new EntrytypeGrouper());
		GROUPER.put(SortKey.TITLE, new TitleGrouper());
		GROUPER.put(SortKey.YEAR, new YearGrouper());
	}

	// all possible bibtex filter fields
	private static final String[] BIBSONOMY_BIBTEX_SELECTION_TYPE_ELEMENTS = {
			PumaBibTeXSelect.ELEMENT_AUTHOR,
			PumaBibTeXSelect.ELEMENT_ENTRYTYPE,
			PumaBibTeXSelect.ELEMENT_TITLE,
			PumaBibTeXSelect.ELEMENT_DOI,
			PumaBibTeXSelect.ELEMENT_ISBN,
			PumaBibTeXSelect.ELEMENT_ISSN,
			PumaBibTeXSelect.ELEMENT_JOURNAL,
			PumaBibTeXSelect.ELEMENT_VOLUME,
			PumaBibTeXSelect.ELEMENT_YEAR,
			PumaBibTeXSelect.ELEMENT_PUBLISHER,
			PumaBibTeXSelect.ELEMENT_ADRESS,
			PumaBibTeXSelect.ELEMENT_EDITION,
			PumaBibTeXSelect.ELEMENT_INSTITUTION,
			PumaBibTeXSelect.ELEMENT_ORGANISATION,
			PumaBibTeXSelect.ELEMENT_SERIES,
			PumaBibTeXSelect.ELEMENT_SCHOOL,
			PumaBibTeXSelect.ELEMENT_LANGUAGE,
			PumaBibTeXSelect.ELEMENT_MISC
	};


	/**
	 * default constructor
	 * call {@link #init(javax.servlet.jsp.PageContext, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 */
	public JspCSLRenderBean() {
		// noop
	}

	static String renderGroupedCSL(final List<Post<BibTex>> posts, final String cslTemplate,
	                               final PublicationGrouper grouper, final boolean showAbstract,
	                               final boolean showBibTeX, final boolean showLink) throws IOException {
		return renderGroupedCSL(posts, cslTemplate, grouper, showAbstract, showBibTeX, showLink, false);
	}

	/**
	 *
	 * @param posts
	 * @param cslTemplate
	 * @param grouper
	 * @param showAbstract
	 * @param showBibTeX
	 * @param showLink
	 * @param showAttachments
	 * @return the rendered output
	 * @throws IOException
	 */
	// TODO: AG 2019-03-01 - Show Attachments
	static String renderGroupedCSL(final List<Post<BibTex>> posts, final String cslTemplate,
	                               final PublicationGrouper grouper, final boolean showAbstract,
	                               final boolean showBibTeX, final boolean showLink, final boolean showAttachments) throws IOException {
		final StringBuilder cslOutput = new StringBuilder();
		final List<CSLItemData> fullConvertedPosts = CSLUtils.convertToCslItemData(posts);
		final Bibliography fullbib = CSLWithLinks.makeAdhocBibliography(
				cslTemplate, "html", fullConvertedPosts.toArray(new CSLItemData[fullConvertedPosts.size()]));
		final String[] ids = fullbib.getEntryIds();

		final Map<String, Integer> idPositionMap = extractIdMap(ids);
		final String[] entries = fullbib.getEntries();
		String lastKey = null;
		for (final Post<BibTex> post : posts) {
			final String postId = CSLUtils.generateIdForPost(post);
			final String output = entries[idPositionMap.get(postId)];
			final String entry = CSLUtils.cleanBib(output);

			if (present(grouper)) {
				final String currentKey = grouper.getLabelForPost(post);
				if (currentKey != null && !currentKey.equals(lastKey)) {
					if (lastKey != null) {
						cslOutput.append(HTML_POSTS_GROUP_ENDING);
					}
					lastKey = currentKey;
					cslOutput.append("<li><h3 class=\"group\">")
							.append(currentKey)
							.append("</h3>")
							.append("<ol>");
				}
			}

			cslOutput.append("<li><div class=\"publication\">").append(entry).append("</div>");

			final String abst = post.getResource().getAbstract();
			final boolean hasAbstract = present(abst) && showAbstract;
			final String url = post.getResource().getUrl();
			final boolean hasLink = present(url) && showLink;

			final String htmlId = "publ_" + postId;
			final String abstractId = htmlId + "_abstract";
			final String bibtexId = htmlId + "_bibtex";
			if (hasAbstract || hasLink || showBibTeX) {
				cslOutput.append("<ul class=\"publication-menu\">");
				if (hasAbstract) {
					cslOutput.append("<li><a href=\"#" + abstractId + "\" onclick=\"toggle('" + abstractId + "', event)\">Abstract</a></li>");
				}
				if (showBibTeX) {
					cslOutput.append("<li><a href=\"#" + bibtexId + "\" onclick=\"toggle('" + bibtexId + "', event)\">BibTeX</a></li>");
				}
				if (hasLink) {
					cslOutput.append("<li><a href=\"" + url + "\">Link</a></li>");
				}
				cslOutput.append("</ul>");
			}

			if (hasAbstract) {
				cslOutput.append("<div class=\"abstract hidden\" id=\"" + abstractId + "\">")
						.append(StringEscapeUtils.escapeHtml(BibTexUtils.cleanBibTex(abst)))
						.append("</div>");
			}
			if (showBibTeX) {
				post.getResource().setInterHash(null);
				post.getResource().setIntraHash(null);
				post.setTags(null);

				cslOutput.append("<div class=\"bibtex hidden\" id=\"" + bibtexId + "\">")
						.append("<textarea rows=\"4\" readonly cols=\"50\">")
						.append(StringEscapeUtils.escapeHtml(BibTexUtils.toBibtexString(post.getResource(), 0)))
						.append("</textarea>")
						.append("</div>");
			}
			cslOutput.append("</li>");
		}
		if (present(grouper)) {
			cslOutput.append(HTML_POSTS_GROUP_ENDING);
		}
		return cslOutput.toString();
	}

	/**
	 * renders the csl based on the settings
	 *
	 * @param parameters
	 * @return the rendered csl
	 */
	public String renderCSL(final Map<String, CmsJspContentAccessValueWrapper> parameters) throws PumaException {
		try {
		BeanParameters beanParameters = new BeanParameters(parameters);

			PublicationsDAO publicationsDAO = PublicationsDAO.createRestPublicationsDAO(
					beanParameters.getApiUrl(),
					beanParameters.getApiUsername(),
					beanParameters.getApiKey());
			final List<Post<BibTex>> posts = publicationsDAO.getPublications(
					beanParameters.getGrouping(),
					beanParameters.getGroupingName(),
					beanParameters.getTags(),
					beanParameters.getSearch(),
					beanParameters.getNumberOfPublications(),
					beanParameters.getRemoveDuplicates());


			PublicationsHtmlFormatter publicationsHtmlFormatter = new PublicationsHtmlFormatter(posts);
			publicationsHtmlFormatter.sort(
					beanParameters.getSortKeys(),
					beanParameters.getSortOrders());


			return publicationsHtmlFormatter.format(
					beanParameters.getCslTemplate(),
					beanParameters.getPublicationGrouper(),
					beanParameters.getShowAbstract(),
					beanParameters.getShowBibTeX(),
					beanParameters.getShowLink());
		} catch (final CmsException e) {
			CONTENT_LOG.error("Error while rendering publications: " + e.getMessage(), e);
			LOG.warn("Exception while rendering publications: " + e.getMessage(), e);
			throw e;
		} catch (final Exception e) {
			CONTENT_LOG.error("Error while rendering publications: " + e.getMessage(), e);
			LOG.warn("Exception while rendering publications: " + e.getMessage(), e);
			throw new PumaException(Messages.get().container(Messages.ERR_EXCEPTION_ON_RENDER_1, e.getMessage()), e);
		}
	}

	/**
	 * @param ids
	 * @return
	 */
	private static Map<String, Integer> extractIdMap(final String[] ids) {
		final Map<String, Integer> map = new HashMap<>();
		int pos = 0;
		for (final String id : ids) {
			map.put(id, pos);
			pos++;
		}
		return map;
	}

	static abstract class PublicationsDAO {
		private LogicInterface logic;

		static PublicationsDAO createRestPublicationsDAO(final String apiUrl,
		                                                 final String apiUserName, final String apiKey) {
			return new RestPublicationsDAO(apiUrl, apiUserName, apiKey);
		}

		public final void setLogic(final LogicInterface logic) {
			this.logic = logic;
		}

		List<Post<BibTex>> getPublications(final GroupingEntity grouping, final String groupingName, final List<String> tags,
		                                   final String search, int numberOfPublications, final boolean removeDuplicates) {

			final List<Post<BibTex>> allPublications = new LinkedList<>();

			int start = 0;
			List<Post<BibTex>> currentList;
			int retrievedPostListSize;
			int numberOfPublicationsToRetrieve;
			// TODO: maybe very inefficient when using removeDuplicates
			do {
				numberOfPublicationsToRetrieve = Math.min(numberOfPublications, Module.MAX_QUERY_SIZE);
				currentList = logic.getPosts(BibTex.class, grouping, groupingName, tags, null, search, null, null, null, null, null, start, start + numberOfPublicationsToRetrieve);
				retrievedPostListSize = currentList.size();
				if (removeDuplicates) {
					// FIXME: AG 2019-02-27: It doesn't remove duplicates from posts from different queries
					// removeDuplicates is O(log n), and it is invoked multiple times during the querying
					// Better use HashSet.add. O(1).
					BibTexUtils.removeDuplicates(currentList);
				}
				allPublications.addAll(currentList);
				start += numberOfPublicationsToRetrieve;
				numberOfPublications -= currentList.size(); // currentList.size != retrievedPostListSize
			} while (retrievedPostListSize == numberOfPublicationsToRetrieve && numberOfPublications > 0);
			return allPublications;
		}

		static class RestPublicationsDAO extends PublicationsDAO {

			private RestPublicationsDAO(final String apiUrl, final String apiUserName, final String apiKey) {
				String apiUrl1 = apiUrl;
				if (!present(apiUrl1)) {
					apiUrl1 = RestLogicFactory.BIBSONOMY_API_URL;
				}
				final LogicInterfaceFactory factory = new RestLogicFactory(apiUrl1);
				setLogic(factory.getLogicAccess(apiUserName, apiKey));
			}
		}
	}

	static class PublicationsHtmlFormatter {
		private final List<Post<BibTex>> posts;

		PublicationsHtmlFormatter(final List<Post<BibTex>> posts) {
			this.posts = posts;
		}

		/**
		 *
		 * @param posts
		 * @param sortKeyList
		 */
		private static void sortPosts(final List<Post<BibTex>> posts, final List<SortKey> sortKeyList,
		                              final List<SortOrder> sortOrders) {
			final BibTexPostComparator comparator = new BibTexPostComparator(sortKeyList, sortOrders);
			posts.sort(comparator);
		}

		public void sort(final List<SortKey> sortKeys, final List<SortOrder> sortOrders) {
			sortPosts(posts, sortKeys, sortOrders);
		}

		public String format(final String cslTemplate, final PublicationGrouper publicationGrouper,
		                     final boolean showAbstract, final boolean showBibTeX, final boolean showLink) throws IOException {
			return format(cslTemplate, publicationGrouper, showAbstract, showBibTeX, showLink, false);
		}

		public String format(final String cslTemplate, final PublicationGrouper publicationGrouper,
		                     final boolean showAbstract, final boolean showBibTeX, final boolean showLink,
		                     final boolean showAttachments) throws IOException {
			return "<ol>" + renderGroupedCSL(posts, cslTemplate, publicationGrouper, showAbstract, showBibTeX, showLink, showAttachments) + "</ol>";
		}
	}

	static class BeanParameters {
		private static final int DEFAULT_NUMBER_OF_PUBLICATIONS = 100;
		private final String apiUrl;
		private final String apiUsername;
		private final String apiKey;
		private GroupingEntity grouping;
		private final String groupingName;
		private final List<String> tags;
		private String search;
		private int numberOfPublications;
		private final boolean removeDuplicates;
		private final List<SortKey> sortKeys;
		private final List<SortOrder> sortOrders;
		private final String cslTemplate;
		private final PublicationGrouper publicationGrouper;
		private final boolean showAbstract;
		private final boolean showBibTeX;
		private final boolean showLink;

		/**
		 * @param parameters
		 * @param key name of the parameter which value is to be read
		 * @return {@code true} if the parameter is defined, not empty, and its string value is {@code true}
		 */
		private static boolean getBooleanParameter(final Map<String, CmsJspContentAccessValueWrapper> parameters, final String key) {
			//		CmsJspContentAccessValueWrapper contentAccessValueWrapper = parameters.get(key);
			//		return !contentAccessValueWrapper.getIsEmptyOrWhitespaceOnly() && contentAccessValueWrapper.getToBoolean();
			final String setting = parameters.get(key).getStringValue();
			if (present(setting)) {
				return Boolean.parseBoolean(setting);
			}

			return false;
		}

		/**
		 * @param sortKey
		 * @param grouped
		 * @return
		 */
		private static PublicationGrouper generateGrouper(final SortKey sortKey, final boolean grouped) {
			if (!grouped) {
				return null;
			}
			return GROUPER.get(sortKey);
		}

		String getApiUrl() {
			return apiUrl;
		}

		BeanParameters(final Map<String, CmsJspContentAccessValueWrapper> parameters) throws PumaException {
			this.apiUrl = parameters.get(PumaPublicationList.ELEMENT_API_HOST).getStringValue();
			this.apiUsername = parseApiUsername(parameters.get(PumaPublicationList.ELEMENT_API_USERNAME).getStringValue());
			this.apiKey = parseApiKey(parameters.get(PumaPublicationList.ELEMENT_API_KEY).getStringValue());

			final String cslStyle = parameters.get(PumaPublicationList.ELEMENT_CSL_STYLE).getStringValue();

			final String tagsInput = parameters.get(PumaPublicationList.ELEMENT_TAGS).getStringValue();
			this.tags = new LinkedList<>();
			if (present(tagsInput)) {
				this.tags.addAll(Arrays.asList(tagsInput.split(TAG_SPLIT_REGEX)));
			}

			final String excludeTags = parameters.get(PumaPublicationList.ELEMENT_EXCLUDE_TAGS).getStringValue();
			if (present(excludeTags)) {
				final String[] excludedTags = excludeTags.split(TAG_SPLIT_REGEX);

				for (final String excludedTag : excludedTags) {
					this.tags.add("sys:not:" + excludedTag);
				}
			}

			this.search = parameters.get(PumaPublicationList.ELEMENT_SEARCH).getStringValue();

			// institute selection
			final String instituteSelection = parameters.get(PumaPublicationList.ELEMENT_INSTITUTE).getStringValue();
			if (present(instituteSelection) && !instituteSelection.equals(InstituteSelectWidget.NO_SELECTION)) {
				// split the tags and add them to the tag list
				this.tags.addAll(Arrays.asList(instituteSelection.split(TAG_SPLIT_REGEX)));
			}

			// add additional search fields
			String concat = "";
			if (!"".equals(this.search)) {
				// add parenthesis to group the search request
				this.search = "(" + this.search + ")";
				concat = " AND ";
			}

			if (parameters.get(PumaPublicationList.ELEMENT_BIBTEX).getExists()) {
				Map<String, CmsJspContentAccessValueWrapper> filterValues = parameters.get(PumaPublicationList.ELEMENT_BIBTEX).getValue();

				// find all added filter elements
				Map<String, String> filterValueList = new HashMap<>();
				for (String filterElement : BIBSONOMY_BIBTEX_SELECTION_TYPE_ELEMENTS) {
					CmsJspContentAccessValueWrapper tmpVal = filterValues.get(filterElement);
					if (tmpVal.getIsSet()) {
						String filterValString = tmpVal.getStringValue();
						if (present(filterValString)) {
							filterValueList.put(filterElement, filterValString);
						}
					}
				}

				for (Map.Entry<String, String> entry : filterValueList.entrySet()) {
					this.search = this.search + concat + "(" + entry.getKey().toLowerCase() + ":" + entry.getValue() + ")";
					concat = " AND ";
				}
			}

			// set search concatenation string
			if (!"".equals(this.search)) {
				concat = " AND ";
			}

			if (parameters.get(PumaPublicationList.ELEMENT_EXCLUDE_BIBTEX).getExists()) {
				Map<String, CmsJspContentAccessValueWrapper> filterValues = parameters.get(PumaPublicationList.ELEMENT_EXCLUDE_BIBTEX).getValue();

				// find all added filter elements
				Map<String, String> filterValueList = new HashMap<>();
				for (String filterElement : BIBSONOMY_BIBTEX_SELECTION_TYPE_ELEMENTS) {
					CmsJspContentAccessValueWrapper tmpVal = filterValues.get(filterElement);
					if (tmpVal.getIsSet()) {
						String filterValString = tmpVal.getStringValue();
						if (present(filterValString)) {
							filterValueList.put(filterElement, filterValString);
						}
					}
				}

				for (Map.Entry<String, String> entry : filterValueList.entrySet()) {
					this.search = this.search + concat + "NOT " + "(" + entry.getKey().toLowerCase() + ":" + entry.getValue() + ")";
					concat = " AND ";
				}
			}

			this.grouping = parseSource(parameters.get(PumaPublicationList.ELEMENT_SOURCE).getStringValue());
			this.groupingName = parameters.get(PumaPublicationList.ELEMENT_SOURCE_ID).getStringValue();
			validateSourceAndSourceID(grouping, groupingName);

			this.numberOfPublications = parseNumberOfPublications(
					parameters.get(PumaPublicationList.ELEMENT_NUMBER_OF_PUBLICATIONS).getStringValue());

			final String manTemplate = parameters.get(PumaPublicationList.ELEMENT_CSL_TEMPLATE).getStringValue();
			final String removeDuplicatesSetting = parameters.get(PumaPublicationList.ELEMENT_REMOVE_DUPLICATES).getStringValue();
			this.removeDuplicates = present(removeDuplicatesSetting) ? Boolean.parseBoolean(removeDuplicatesSetting) : false;

			this.cslTemplate = present(manTemplate) ? manTemplate : Layouts.getCSLTemplate(cslStyle);
			LOG.debug("Using template: " + this.cslTemplate);

			// XXX: ... posts = getPublications ...

			boolean grouped = getBooleanParameter(parameters, PumaPublicationList.ELEMENT_GROUP_PUBLICATIONS);
			final String sortKeySetting = parameters.get(PumaPublicationList.ELEMENT_SORT_KEY).getStringValue();
			SortKey sortKey;
			sortKey = present(sortKeySetting) ? SortKey.valueOf(sortKeySetting) : SortKey.NONE;
			// old settings
			final CmsJspContentAccessValueWrapper oldYearGroupSetting = parameters.get(ELEMENT_YEAR_GROUPING);
			if (present(oldYearGroupSetting)) {
				final boolean active = Boolean.parseBoolean(oldYearGroupSetting.getStringValue());
				if (active) {
					grouped = true;
					sortKey = SortKey.YEAR;
				}
			}
			this.sortKeys = new ArrayList<>();
			this.sortOrders = new ArrayList<>();

			final String sortOrderSetting = parameters.get(PumaPublicationList.ELEMENT_SORT_ORDER).getStringValue();
			final SortOrder sortOrder = present(sortOrderSetting) ? SortOrder.valueOf(sortOrderSetting) : SortOrder.ASC;


			this.sortKeys.add(sortKey);
			this.sortOrders.add(sortOrder);

			// get the second level sort criteria
			final String sortKeySetting2 = parameters.get(PumaPublicationList.ELEMENT_SORT_KEY2).getStringValue();
			if (present(sortKeySetting2)) {
				SortKey sortKey2;
				sortKey2 = SortKey.valueOf(sortKeySetting2);

				final String sortOrderSetting2 = parameters.get(PumaPublicationList.ELEMENT_SORT_ORDER2).getStringValue();
				final SortOrder sortOrder2 = present(sortOrderSetting2) ? SortOrder.valueOf(sortOrderSetting2) : SortOrder.ASC;
				// add to list
				this.sortKeys.add(sortKey2);
				this.sortOrders.add(sortOrder2);
			}

			// get the third level sort criteria
			final String sortKeySetting3 = parameters.get(PumaPublicationList.ELEMENT_SORT_KEY3).getStringValue();
			if (present(sortKeySetting3)) {
				SortKey sortKey3;
				sortKey3 = SortKey.valueOf(sortKeySetting3);

				final String sortOrderSetting3 = parameters.get(PumaPublicationList.ELEMENT_SORT_ORDER3).getStringValue();
				final SortOrder sortOrder3 = present(sortOrderSetting3) ? SortOrder.valueOf(sortOrderSetting3) : SortOrder.ASC;
				// add to list
				this.sortKeys.add(sortKey3);
				this.sortOrders.add(sortOrder3);
			}

			// XXX: sortPosts...

			this.showAbstract = getBooleanParameter(parameters, PumaPublicationList.ELEMENT_SHOW_ABSTRACT);
			this.showBibTeX = getBooleanParameter(parameters, PumaPublicationList.ELEMENT_SHOW_BIBTEX);
			this.showLink = getBooleanParameter(parameters, PumaPublicationList.ELEMENT_SHOW_LINK);

			this.publicationGrouper = generateGrouper(sortKey, grouped);
		}

		String getApiKey() {
			return apiKey;
		}

		GroupingEntity getGrouping() {
			return grouping;
		}

		String getGroupingName() {
			return groupingName;
		}

		List<String> getTags() {
			return tags;
		}

		String getSearch() {
			return search;
		}

		int getNumberOfPublications() {
			return numberOfPublications;
		}

		boolean getRemoveDuplicates() {
			return removeDuplicates;
		}

		List<SortKey> getSortKeys() {
			return sortKeys;
		}

		List<SortOrder> getSortOrders() {
			return sortOrders;
		}

		String getCslTemplate() {
			return cslTemplate;
		}

		PublicationGrouper getPublicationGrouper() {
			return publicationGrouper;
		}

		boolean getShowAbstract() {
			return showAbstract;
		}

		boolean getShowBibTeX() {
			return showBibTeX;
		}

		boolean getShowLink() {
			return showLink;
		}

		/**
		 * @throws PumaException if no valid {@code apiUsername} has been provided
		 */
		public static String parseApiUsername(String apiUsername) throws PumaException {
			if (!present(apiUsername)) {
				throw new PumaException(Messages.get().container(Messages.ERR_INVALID_API_USER_0));
			}
			return apiUsername;

		}

		/**
		 * @throws PumaException if no valid @{code apiKey} has been provided
		 */
		public static String parseApiKey(String apiKey) throws PumaException {
			if (!present(apiKey)) {
				throw new PumaException(Messages.get().container(Messages.ERR_INVALID_API_KEY_0));
			}
			return apiKey;
		}

		/**
		 * @param numberOfPublications  a String containing the int representation to be parsed
		 * @return the integer value represented by the argument in decimal.
		 * @throws PumaException if the string {@code numberOfPublications} has been provided but is not a valid number
		 */
		public static int parseNumberOfPublications(String numberOfPublications) throws PumaException {
			int result = DEFAULT_NUMBER_OF_PUBLICATIONS;
			if (present(numberOfPublications)) {
				try {
					result = Integer.parseInt(numberOfPublications.trim());
				} catch (final NumberFormatException e) {
					throw new PumaException(Messages.get().container(Messages.ERR_INVALID_NUMBER_OF_PUBLICATIONS_0));
				}
			}
			return result;
		}

		static GroupingEntity parseSource(final String groupingString) {
			final GroupingEntity result;
			switch (groupingString.toUpperCase()) {
				case SourceSelectWidget.GROUP_SOURCE:
					result = GroupingEntity.GROUP;
					break;
				case SourceSelectWidget.USER_SOURCE:
					result = GroupingEntity.USER;
					break;
				default:
					result = GroupingEntity.ALL;
					break;
			}

			return result;
		}

		/**
		 * @param source content source
		 * @param sourceID id of user, group or viewable source
		 * @throws PumaException if no valid {@code sourceID} provided for the selected @{code source}
		 */
		static void validateSourceAndSourceID(GroupingEntity source, String sourceID) throws PumaException {
			if ((source != GroupingEntity.ALL) && !present(sourceID)) {
				throw new PumaException(Messages.get().container(Messages.ERR_SOURCE_ID_REQUIRED_0));
			}
		}

		String getApiUsername() {
			return apiUsername;
		}
	}
}
