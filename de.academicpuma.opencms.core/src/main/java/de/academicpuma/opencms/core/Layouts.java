package de.academicpuma.opencms.core;

import net.sf.ehcache.store.chm.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentMap;

public class Layouts {
	private static final Log LOG = CmsLog.getLog(Layouts.class);
	private static final ConcurrentMap<String, String> LAYOUT_CACHE = new ConcurrentHashMap<String, String>();

	/**
	 * Caches and returns the content of a csl template file.
	 *
	 * @param cslStyle template filename (without extension). E.g.: {@code din-1505-2}.
	 * @return the content of the csl template file
	 */
	public static String getCSLTemplate(final String cslStyle) {
		if (LAYOUT_CACHE.containsKey(cslStyle)) {
			return LAYOUT_CACHE.get(cslStyle);
		}
		String filename = cslStyle + ".csl";
		try {
			final InputStream resourceAsStream = Layouts.class.getClassLoader().getResourceAsStream(filename);
			if (resourceAsStream == null) {
				throw new IllegalStateException("csl file not found");
			}

			final String cslTemplate = readFromInputStream(resourceAsStream);

			LAYOUT_CACHE.put(cslStyle, cslTemplate);
			resourceAsStream.close();
			return cslTemplate;
		} catch (final IOException e) {
			LOG.error("Error reading " + filename + ": " + e.getLocalizedMessage(), e);
			throw new IllegalStateException("Error reading " + filename + ": " + e.getLocalizedMessage(), e);
		}
	}

	/**
	 * @param resourceAsStream
	 * @return the content read from {@code resourceAsStream}
	 */
	private static String readFromInputStream(InputStream resourceAsStream) throws IOException {
		final BufferedReader reader = new BufferedReader(new InputStreamReader(resourceAsStream));
		final StringBuilder content = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			content.append(line).append("\n");
		}

		return content.toString();
	}
}
