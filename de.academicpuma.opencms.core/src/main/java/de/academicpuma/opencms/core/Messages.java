package de.academicpuma.opencms.core;

import org.apache.commons.logging.Log;
import org.opencms.i18n.A_CmsMessageBundle;
import org.opencms.i18n.I_CmsMessageBundle;
import org.opencms.main.CmsLog;

/**
 * the messages for this module
 *
 * @author dzo
 * @author alberto
 */
public class Messages extends A_CmsMessageBundle {

	public static final String ERR_INVALID_API_USER_0 = "ERR_INVALID_API_USER_0";
	public static final String ERR_INVALID_API_KEY_0 = "ERR_INVALID_API_KEY_0";
	public static final String ERR_INVALID_NUMBER_OF_PUBLICATIONS_0 = "ERR_INVALID_NUMBER_OF_PUBLICATIONS_0";
	public static final String ERR_EXCEPTION_ON_RENDER_1 = "ERR_EXCEPTION_ON_RENDER_1";
	public static final String ERR_SOURCE_ID_REQUIRED_0 = "ERR_SOURCE_ID_REQUIRED_0";

	private static final String BUNDLE_NAME = Messages.class.getPackage().getName() + ".messages";

	/** Static instance member. */
	private static final I_CmsMessageBundle INSTANCE = new Messages();

	/**
	 * @return an instance of this localized message accessor
	 */
	public static I_CmsMessageBundle get() {
		return INSTANCE;
	}
	
	private Messages() {
		// noop
	}
	
	/* (non-Javadoc)
	 * @see org.opencms.i18n.I_CmsMessageBundle#getBundleName()
	 */
	@Override
	public String getBundleName() {
		return BUNDLE_NAME;
	}

}
