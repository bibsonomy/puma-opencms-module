package de.academicpuma.opencms.core;

import org.apache.commons.logging.Log;
import org.opencms.configuration.CmsConfigurationManager;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsLog;
import org.opencms.module.A_CmsModuleAction;
import org.opencms.module.CmsModule;

/**
 * the main module class
 *
 * @author dzo
 */
public class Module extends A_CmsModuleAction {
	private static final Log LOG = CmsLog.getLog(Module.class);

	// AG 2018-09-10: TODO: BibSonomy transitioned from const to configurable setting in commit#65067f168f7a3da30d1a6cc965dbe09eff010e04
	// To keep consistency, this const should also be changed into a configurable property
	public static final int MAX_QUERY_SIZE = 1000;

	/** the name of the module */
	public static final String MODULE_NAME = Module.class.getPackage().getName();

	@Override
	public void initialize(CmsObject adminCms, CmsConfigurationManager configurationManager, CmsModule module) {
		super.initialize(adminCms, configurationManager, module);
		LOG.info("Module '" + MODULE_NAME + "' initialized");
	}

}
