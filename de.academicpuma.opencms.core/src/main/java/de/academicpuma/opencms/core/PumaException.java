package de.academicpuma.opencms.core;

import org.opencms.file.CmsObject;
import org.opencms.i18n.CmsMessageContainer;
import org.opencms.main.CmsException;
import org.opencms.main.OpenCms;

public class PumaException extends CmsException {
	public PumaException(final CmsMessageContainer message) {
		super(message);
	}

	public PumaException(final CmsMessageContainer message, final Throwable cause) {
		super(message, cause);
	}

	public String getLocalizedMessage(CmsObject cms) {
		return this.getLocalizedMessage(OpenCms.getWorkplaceManager().getWorkplaceLocale(cms));
	}
}
