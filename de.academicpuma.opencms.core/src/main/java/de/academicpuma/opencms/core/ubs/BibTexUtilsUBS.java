package de.academicpuma.opencms.core.ubs;

import java.util.Arrays;
import java.util.List;

public class BibTexUtilsUBS {
	private static final String BACHELOR_THESIS = "Bachelorarbeit";
	private static final String MASTER_THESIS = "Masterarbeit";
	private static final String RESEARCH_PAPER = "Forschungsarbeit";
	private static final String STUDY_PAPER = "Studienarbeit";
	private static final String DIPLOMA = "Diplomarbeit";

	public static final List<String> PUBLICATION_TYPES = Arrays.asList(
			BACHELOR_THESIS,
			MASTER_THESIS,
			RESEARCH_PAPER,
			STUDY_PAPER,
			DIPLOMA
	);
}
