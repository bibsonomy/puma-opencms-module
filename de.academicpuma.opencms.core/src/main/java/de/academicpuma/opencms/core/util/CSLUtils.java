package de.academicpuma.opencms.core.util;

import de.academicpuma.opencms.core.ubs.BibTexUtilsUBS;
import de.undercouch.citeproc.bibtex.PageParser;
import de.undercouch.citeproc.bibtex.PageRange;
import de.undercouch.citeproc.csl.CSLDateBuilder;
import de.undercouch.citeproc.csl.CSLItemData;
import de.undercouch.citeproc.csl.CSLItemDataBuilder;
import de.undercouch.citeproc.csl.CSLName;
import de.undercouch.citeproc.csl.CSLNameBuilder;
import de.undercouch.citeproc.csl.CSLType;

import org.bibsonomy.common.exceptions.InvalidModelException;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.model.util.TagUtils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.bibsonomy.util.ValidationUtils.present;

/**
 * Utils for the CSL renderer
 *
 * @author dzo
 */
public final class CSLUtils {
	private static final Pattern CONTENT_PATTERN = Pattern.compile("<div class=\"csl-right-inline\">(.+?)</div>", Pattern.DOTALL);

	/** BibTex entry type -> CSL type */
	private static final Map<String, CSLType> TYPEMAP;

	static {
		TYPEMAP = new HashMap<>();

		TYPEMAP.put(BibTexUtils.ARTICLE, CSLType.ARTICLE_JOURNAL);

		TYPEMAP.put(BibTexUtils.BOOK, CSLType.BOOK);
		TYPEMAP.put(BibTexUtils.PROCEEDINGS, CSLType.BOOK);
		TYPEMAP.put(BibTexUtils.PERIODICAL, CSLType.BOOK);
		TYPEMAP.put(BibTexUtils.MANUAL, CSLType.BOOK);

		TYPEMAP.put(BibTexUtils.BOOKLET, CSLType.PAMPHLET);

		TYPEMAP.put(BibTexUtils.INBOOK, CSLType.CHAPTER);
		TYPEMAP.put(BibTexUtils.INCOLLECTION, CSLType.CHAPTER);

		TYPEMAP.put(BibTexUtils.INPROCEEDINGS, CSLType.PAPER_CONFERENCE);
		TYPEMAP.put(BibTexUtils.CONFERENCE, CSLType.PAPER_CONFERENCE);

		TYPEMAP.put(BibTexUtils.PHD_THESIS, CSLType.THESIS);
		TYPEMAP.put(BibTexUtils.MASTERS_THESIS, CSLType.THESIS);

		TYPEMAP.put(BibTexUtils.TECH_REPORT, CSLType.REPORT);

		TYPEMAP.put(BibTexUtils.PATENT, CSLType.PATENT);

		TYPEMAP.put(BibTexUtils.ELECTRONIC, CSLType.WEBPAGE);

		TYPEMAP.put(BibTexUtils.MISC, CSLType.ARTICLE);

		TYPEMAP.put(BibTexUtils.STANDARD, CSLType.LEGISLATION);

		TYPEMAP.put(BibTexUtils.UNPUBLISHED, CSLType.MANUSCRIPT);
		TYPEMAP.put(BibTexUtils.PREPRINT, CSLType.MANUSCRIPT);

		// XXX: Mappings missing for elements from https://www.bibsonomy.org/help_en/Entrytypes:
		// - collection
		// - dataset
		// - preamble
		// - presentation
		// - techreport
	}
	
	/**
	 * @param posts
	 * @return a map with the converted posts
	 * @see #convertToCslItemData(List)
	 */
	public static Map<String, CSLItemData> convertToCSLItemDataMap(List<Post<BibTex>> posts) {
		final Map<String, CSLItemData> map = new HashMap<>();

		for (final Post<BibTex> post : posts) {
			final CSLItemData cslItem = convertPost(post);
			map.put(cslItem.getId(), cslItem);
		}

		return map;
	}

	/**
	 * converts the posts to {@link CSLItemData}
	 *
	 * @param posts
	 * @return the converted posts
	 */
	public static List<CSLItemData> convertToCslItemData(List<Post<BibTex>> posts) {
		final List<CSLItemData> list = new LinkedList<>();

		for (final Post<BibTex> post : posts) {
			final CSLItemData cslItem = convertPost(post);
			list.add(cslItem);
		}

		return list;
	}

	/**
	 * Convert the publication contained in the bibsonomy {@code post} into a CSL Item.
	 *
	 * @param post the bibsonomy post
	 * @return the converted post publication as {@link CSLItemData}
	 * @see <a href="https://www.bibsonomy.org/help_en/Entrytypes">bibsonomy entry types</a>
	 * @see <a href="http://docs.citationstyles.org/en/stable/specification.html#appendix-iii-types">CSL Types</a>
	 */
	public static CSLItemData convertPost(final Post<BibTex> post) {
		final BibTex publication = post.getResource();

		/*
		 * prepare misc fields ignore exception
		 */
		try {
			publication.parseMiscField();
		} catch (final InvalidModelException e) {
			// ignore
		}

		final String id = generateIdForPost(post);
		final CSLItemDataBuilder cslDataBuilder = new CSLItemDataBuilder();
		final CSLName[] editors = getCSLNames(publication.getEditor());
		cslDataBuilder.id(id);

		cslDataBuilder.type(getCSLType(publication.getEntrytype()))
				.author(getCSLNames(publication.getAuthor()))
				.editor(editors).collectionEditor(editors).containerAuthor(editors);

		// mapping address
		final String venue = publication.getMiscField("venue");
		final String location = publication.getMiscField("location");
		final String address = publication.getAddress();
		if (present(venue)) {
			cslDataBuilder.eventPlace(BibTexUtils.cleanBibTex(venue));
			final String eventtitle = publication.getMiscField("eventtitle");
			if (present(eventtitle)) {
				cslDataBuilder.event(BibTexUtils.cleanBibTex(eventtitle));
			}
		} else if (present(location)) {
			final String cleanedLocation = BibTexUtils.cleanBibTex(location);
			cslDataBuilder
					.eventPlace(cleanedLocation)
					.publisherPlace(cleanedLocation);
		} else if (present(address)){
			final String cleanedAddress = BibTexUtils.cleanBibTex(address);
			cslDataBuilder
					.eventPlace(cleanedAddress)
					.publisherPlace(cleanedAddress);
		}

		// mapping bibtexkey
		cslDataBuilder.citationLabel(BibTexUtils.cleanBibTex(publication.getBibtexKey()));

		// mapping journal, booktitle and series
		final String cleanedJournal = BibTexUtils.cleanBibTex(publication.getJournal());
		final String cleanedBooktitle = BibTexUtils.cleanBibTex(publication.getBooktitle());
		final String cleanedSeries = BibTexUtils.cleanBibTex(publication.getSeries());
		final String colTitleToUse;
		if (present(cleanedJournal)) {
			colTitleToUse = cleanedJournal;
		} else if (present(cleanedBooktitle)) {
			colTitleToUse = cleanedBooktitle;
		} else {
			colTitleToUse = cleanedSeries;
		}

		cslDataBuilder.containerTitle(colTitleToUse);
		cslDataBuilder.collectionTitle(colTitleToUse);

		// mapping publisher, techreport, thesis, organization
		if (present(publication.getPublisher())) {
			cslDataBuilder.publisher(BibTexUtils.cleanBibTex(publication.getPublisher()));
		} else if (BibTexUtils.TECH_REPORT.equals(publication.getEntrytype())) {
			cslDataBuilder.publisher(BibTexUtils.cleanBibTex(publication.getInstitution()));
		} else if (BibTexUtils.PHD_THESIS.equals(publication.getEntrytype())) {
			cslDataBuilder.publisher(BibTexUtils.cleanBibTex(publication.getSchool()));
		} else if (BibTexUtilsUBS.PUBLICATION_TYPES.contains(publication.getType())) {
			if ((publication.getSchool() != null) || (publication.getInstitution() != null)) {
				boolean schoolAndInstitutionPresent = (publication.getSchool() != null) && (publication.getInstitution() != null);
				cslDataBuilder.publisher(
						((publication.getSchool() == null) ? "" : BibTexUtils.cleanBibTex(publication.getSchool()))
								+ ((schoolAndInstitutionPresent) ? ", " : "")
								+ ((publication.getInstitution() == null) ? "" : BibTexUtils.cleanBibTex(publication.getInstitution()))
				);
			}
		} else {
			cslDataBuilder.publisher(BibTexUtils.cleanBibTex(publication.getOrganization()));
		}

		// map genre as per https://aurimasv.github.io/z2csl/typeMap.xml#map-thesis
		if (present((publication.getEntrytype()))) {
			switch (publication.getEntrytype()) {
				case BibTexUtils.BOOK:
				case BibTexUtils.ELECTRONIC:
				case BibTexUtils.MANUAL:
				case BibTexUtils.MASTERS_THESIS:
				case BibTexUtils.PERIODICAL:
				case BibTexUtils.PHD_THESIS:
				case BibTexUtils.PROCEEDINGS:
				case BibTexUtils.TECH_REPORT:
				case BibTexUtils.UNPUBLISHED:
					cslDataBuilder.genre(publication.getType());
					break;
				default:
					// ignore genre
					break;
			}
		}

		// mapping chapter
		final String chapter = publication.getChapter();
		if (present(chapter)) {
			cslDataBuilder.chapterNumber(BibTexUtils.cleanBibTex(chapter));
		}

		// mapping title
		final String title = BibTexUtils.cleanBibTex(publication.getTitle());
		if (present(title)) {
			cslDataBuilder.title(title);
		} else {
			// XXX: title is a required field
			cslDataBuilder.title(chapter);
		}

		// mapping number
		final String cleanedNumber = BibTexUtils.cleanBibTex(publication.getNumber());
		cslDataBuilder.number(cleanedNumber);

		final String cleanedIssue = BibTexUtils.cleanBibTex(publication.getMiscField("issue"));
		final String issueToUse;
		if (present(cleanedIssue)) {
			issueToUse = cleanedIssue;
		} else {
			issueToUse = cleanedNumber;
		}
		cslDataBuilder.issue(issueToUse);

		final String accessed = BibTexUtils.cleanBibTex(publication.getMiscField("accessed"));
		if (present(accessed)) {
			final CSLDateBuilder accessedDateBuilder = new CSLDateBuilder();
			accessedDateBuilder.literal(accessed);
			cslDataBuilder.accessed(accessedDateBuilder.build());
		}

		// date mapping
		final String urlDate = BibTexUtils.cleanBibTex(publication.getMiscField("urldate"));
		final String cleanedDate = BibTexUtils.cleanBibTex(publication.getMiscField("date"));
		final CSLDateBuilder dateBuilder = new CSLDateBuilder();
		if (BibTexUtils.ELECTRONIC.equals(publication.getEntrytype()) && present(urlDate)) {
			dateBuilder.raw(urlDate);
		} else if (present(cleanedDate)) {
			dateBuilder.raw(cleanedDate);
			cslDataBuilder.eventDate(dateBuilder.build());
		} else {
			try {
				final int year = Integer.parseInt(publication.getYear());
				dateBuilder.dateParts(year);

				final String cleanedMonth = BibTexUtils.cleanBibTex(publication.getMonth());
				int month = 0;
				if (present(cleanedMonth)) {
					month = Integer.parseInt(cleanedMonth);
					dateBuilder.dateParts(year, month);
				}

				final String cleanedDay = BibTexUtils.cleanBibTex(publication.getDay());
				if (present(cleanedDay)) {
					dateBuilder.dateParts(year, month, Integer.parseInt(cleanedDay));
				}
			} catch (final NumberFormatException e) {
				// ignore it
				dateBuilder.raw(publication.getYear());
			}
		}
		cslDataBuilder.issued(dateBuilder.build());

		final String cleanedPages = BibTexUtils.cleanBibTex(publication.getPages());
		cslDataBuilder.page(cleanedPages);

		if (present(cleanedPages)) {
			final PageRange pageRange = PageParser.parse(cleanedPages);
			cslDataBuilder.pageFirst(pageRange.getPageFirst());

			Integer numberOfPages = pageRange.getNumberOfPages();
			if (present(numberOfPages)) {
				cslDataBuilder.numberOfPages(String.valueOf(numberOfPages));
			}
		}

		final String language = BibTexUtils.cleanBibTex(publication.getMiscField("language"));
		if (present(language)) {
			cslDataBuilder.language(language);
		}

		if (publication.getEdition() != null) {
			cslDataBuilder.edition(BibTexUtils.cleanBibTex(publication.getEdition()));
		}

		cslDataBuilder.volume(BibTexUtils.cleanBibTex(publication.getVolume()))
				.keyword(TagUtils.toTagString(post.getTags(), " "))
				.URL(BibTexUtils.cleanBibTex(publication.getUrl()))
				.status(BibTexUtils.cleanBibTex(publication.getMiscField("status")))
				.ISBN(BibTexUtils.cleanBibTex(publication.getMiscField("isbn")))
				.ISSN(BibTexUtils.cleanBibTex(publication.getMiscField("issn")))
				.version(BibTexUtils.cleanBibTex(publication.getMiscField("revision")))
				.annote(BibTexUtils.cleanBibTex(publication.getAnnote()))
				.abstrct(publication.getAbstract())
				.DOI(BibTexUtils.cleanBibTex(publication.getMiscField("doi")))
				.note(BibTexUtils.cleanBibTex(publication.getNote()));

		return cslDataBuilder.build();
	}

	/**
	 * @param post
	 * @return the post
	 */
	public static String generateIdForPost(final Post<BibTex> post) {
		return post.getResource().getIntraHash() + post.getUser().getName();
	}

	/**
	 * @param author
	 * @return
	 */
	private static CSLName[] getCSLNames(List<PersonName> author) {
		if (!present(author)) {
			return null;
		}

		final CSLName[] cslNames = new CSLName[author.size()];
		for (int i = 0; i < author.size(); i++) {
			final PersonName personName = author.get(i);
			final CSLNameBuilder nameBuilder = new CSLNameBuilder();
			nameBuilder
					.given(BibTexUtils.cleanBibTex(personName.getFirstName()))
					.family(BibTexUtils.cleanBibTex(personName.getLastName()));
			cslNames[i] = nameBuilder.build();
		}

		return cslNames;
	}

	/**
	 * @param entrytype BibTex entry type
	 * @return corresponding CSL type
	 */
	private static CSLType getCSLType(String entrytype) {
		return TYPEMAP.get(entrytype);
	}

	/**
	 * removes the numbers from the rendered output
	 *
	 * @param cslOutPut
	 * @return cslOutPut without list numbers
	 */
	public static String cleanBib(final String cslOutPut) {
		final Matcher matcher = createMatcher(cslOutPut);
		if (matcher.find()) {
			return matcher.group(1).trim();
		}
		return cslOutPut;
	}

	/**
	 * @param cslOutPut
	 * @return
	 */
	private static Matcher createMatcher(final String cslOutPut) {
		return CONTENT_PATTERN.matcher(cslOutPut);
	}
}
