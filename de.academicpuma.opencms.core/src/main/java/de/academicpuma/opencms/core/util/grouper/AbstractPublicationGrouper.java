package de.academicpuma.opencms.core.util.grouper;

import static org.bibsonomy.util.ValidationUtils.present;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.util.BibTexUtils;

/**
 * abstract implementation
 * @author dzo
 */
public abstract class AbstractPublicationGrouper implements PublicationGrouper {
	
	/* (non-Javadoc)
	 * @see PublicationGrouper#getLabelKeyForPost(org.bibsonomy.model.Post)
	 */
	@Override
	public final String getLabelForPost(Post<BibTex> post) {
		final String label = BibTexUtils.cleanBibTex(this.getLabelForResource(post));
		if (present(label)) {
			return this.modifyLabel(label);
		}
		return this.getDefaultLabel();
	}

	/**
	 * noop implementation
	 * @param label
	 * @return the label
	 */
	protected String modifyLabel(String label) {
		return label;
	}

	/**
	 * @return the default label if the actual label is empty
	 */
	protected String getDefaultLabel() {
		return null;
	}

	/**
	 * @param post
	 * @return the label for the post
	 */
	protected abstract String getLabelForResource(Post<BibTex> post);
}
