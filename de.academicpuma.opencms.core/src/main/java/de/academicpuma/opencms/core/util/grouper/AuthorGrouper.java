package de.academicpuma.opencms.core.util.grouper;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.List;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.Post;

/**
 * group publications by {@link BibTex#getAuthor()}
 * 
 * @author dzo
 */
public class AuthorGrouper extends AbstractPublicationGrouper {

	/* (non-Javadoc)
	 * @see AbstractPublicationGrouper#getLabelForResource(org.bibsonomy.model.Post)
	 */
	@Override
	protected String getLabelForResource(Post<BibTex> post) {
		final BibTex resource = post.getResource();
		final List<PersonName> authors = resource.getAuthor();
		if (present(authors)) {
			return authors.get(0).getLastName();
		}
		
		final List<PersonName> editors = resource.getEditor();
		if (present(editors)) {
			return editors.get(0).getLastName();
		}
		
		return null;
	}
	
	/* (non-Javadoc)
	 * @see AbstractPublicationGrouper#getDefaultLabel()
	 */
	@Override
	protected String getDefaultLabel() {
		return "no authors";
	}
	
	/* (non-Javadoc)
	 * @see AbstractPublicationGrouper#modifyLabel(java.lang.String)
	 */
	@Override
	protected String modifyLabel(String label) {
		return label.substring(0, 1).toUpperCase();
	}
}
