package de.academicpuma.opencms.core.util.grouper;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;

/**
 * grouping publications by {@link BibTex#getEntrytype()}
 *
 * @author dzo
 */
public class EntrytypeGrouper extends AbstractPublicationGrouper {

	/* (non-Javadoc)
	 * @see AbstractPublicationGrouper#getLabelForResource(org.bibsonomy.model.Post)
	 */
	@Override
	protected String getLabelForResource(Post<BibTex> post) {
		return post.getResource().getEntrytype();
	}

}
