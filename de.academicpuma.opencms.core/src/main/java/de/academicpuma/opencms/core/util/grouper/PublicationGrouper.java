package de.academicpuma.opencms.core.util.grouper;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;

/**
 * @author dzo
 */
public interface PublicationGrouper {
	
	/**
	 * get the label for the post
	 * @param post
	 * @return the label for the post
	 */
	public String getLabelForPost(final Post<BibTex> post);
}
