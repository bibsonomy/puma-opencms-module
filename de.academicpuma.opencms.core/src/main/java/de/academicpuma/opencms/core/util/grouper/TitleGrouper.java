package de.academicpuma.opencms.core.util.grouper;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;

/**
 * group publications by {@link BibTex#getTitle()}
 *
 * @author dzo
 */
public class TitleGrouper extends AbstractPublicationGrouper {
	
	/* (non-Javadoc)
	 * @see AbstractPublicationGrouper#getLabelForResource(org.bibsonomy.model.Post)
	 */
	@Override
	protected String getLabelForResource(Post<BibTex> post) {
		return post.getResource().getTitle();
	}
	
	/* (non-Javadoc)
	 * @see AbstractPublicationGrouper#modifyLabel(java.lang.String)
	 */
	@Override
	protected String modifyLabel(String label) {
		return label.substring(0, 1).toUpperCase();
	}
}