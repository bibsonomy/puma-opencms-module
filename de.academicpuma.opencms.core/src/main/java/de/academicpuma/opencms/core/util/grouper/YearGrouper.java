package de.academicpuma.opencms.core.util.grouper;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;

/**
 * group publications by {@link BibTex#getYear()}
 *
 * @author dzo
 */
public class YearGrouper extends AbstractPublicationGrouper {

	/* (non-Javadoc)
	 * @see AbstractPublicationGrouper#getLabelForResource(org.bibsonomy.model.Post)
	 */
	@Override
	protected String getLabelForResource(Post<BibTex> post) {
		return post.getResource().getYear();
	}

}
