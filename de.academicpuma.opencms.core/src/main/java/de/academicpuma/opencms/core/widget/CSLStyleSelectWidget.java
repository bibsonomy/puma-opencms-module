package de.academicpuma.opencms.core.widget;

import java.util.LinkedList;
import java.util.List;

import de.academicpuma.opencms.core.CSLStyles;
import org.opencms.widgets.CmsSelectWidget;
import org.opencms.widgets.CmsSelectWidgetOption;
import org.opencms.widgets.I_CmsWidget;

/**
 * @author dzo
 */
public class CSLStyleSelectWidget extends CmsSelectWidget {
	
	private static final List<CmsSelectWidgetOption> getAllCslSyles() {
		final List<CmsSelectWidgetOption> styleOptions = new LinkedList<CmsSelectWidgetOption>();
		// TODO: sort the styles
		for (final CSLStyles cslStyle : CSLStyles.values()) {
			final CmsSelectWidgetOption cslstyleOption = new CmsSelectWidgetOption(cslStyle.getFilename(), false, cslStyle.getName());
			styleOptions.add(cslstyleOption);
		}
		return styleOptions;
	}
	
	/**
	 * default constructor
	 */
	public CSLStyleSelectWidget() {
		super(getAllCslSyles());
	}

	/**
	 * @param configuration
	 */
	public CSLStyleSelectWidget(String configuration) {
		super(getAllCslSyles());
	}

	/* (non-Javadoc)
	 * @see org.opencms.widgets.I_CmsWidget#newInstance()
	 */
	@Override
	public I_CmsWidget newInstance() {
		return new CSLStyleSelectWidget(getConfiguration());
	}
}
