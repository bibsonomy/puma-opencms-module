package de.academicpuma.opencms.core.widget;

import static de.academicpuma.opencms.core.Module.MAX_QUERY_SIZE;
import static org.bibsonomy.util.ValidationUtils.present;

import org.apache.commons.logging.Log;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.opencms.main.CmsLog;
import org.opencms.main.OpenCms;
import org.opencms.widgets.CmsSelectWidget;
import org.opencms.widgets.CmsSelectWidgetOption;
import org.opencms.widgets.I_CmsWidget;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.academicpuma.opencms.core.JspCSLRenderBean;
import de.academicpuma.opencms.core.Module;

/**
 * Select widget that holds all possible institutes
 * @author mho
 */
public class InstituteSelectWidget extends CmsSelectWidget {
	
	public final static String NO_SELECTION = "None";
	
	private static final Log log = CmsLog.getLog(JspCSLRenderBean.class);
	
	/**
	 * default constructor
	 */
	public InstituteSelectWidget() {
		super(getAllInstitutes());
	}

	/**
	 * @param configuration
	 */
	public InstituteSelectWidget(String configuration) {
		super(getAllInstitutes());
	}

	/* (non-Javadoc)
	 * @see org.opencms.widgets.I_CmsWidget#newInstance()
	 */
	@Override
	public I_CmsWidget newInstance() {
		return new InstituteSelectWidget(getConfiguration());
	}
	
	
	private static final List<CmsSelectWidgetOption> getAllInstitutes() {
		final List<CmsSelectWidgetOption> instituteOptions = new LinkedList<CmsSelectWidgetOption>();
		
		OpenCms.getModuleManager().getModule(Module.MODULE_NAME).getParameter("apiUrl");
		
		final String apiUrl = OpenCms.getModuleManager().getModule(Module.MODULE_NAME).getParameter("apiUrl");
		final String apiUserName = OpenCms.getModuleManager().getModule(Module.MODULE_NAME).getParameter("apiUserName");
		final String apiKey =OpenCms.getModuleManager().getModule(Module.MODULE_NAME).getParameter("apiKey");
		final String identifyTag = OpenCms.getModuleManager().getModule(Module.MODULE_NAME).getParameter("identifyTag");
		
		GroupingEntity grouping = GroupingEntity.USER;
		String groupingName = apiUserName;
		String search = "";
		int numberOfPublications = 10000;
		boolean removeDuplicates = true;
		List<String> tags = new ArrayList<>();
		
		tags.add(identifyTag);
		
		// add empty option
		instituteOptions.add(new CmsSelectWidgetOption(NO_SELECTION, true, "NONE"));
		
		try {
			List<Post<Bookmark>> instututes = getBookmarks(apiUrl, apiUserName, apiKey, grouping, groupingName, tags, search, numberOfPublications, removeDuplicates);
			
			// TODO: sort the institutes
			for (final Post<Bookmark> instutute : instututes) {
				String tagString = "";
				// remove the identifier tag - TODO evaluate if all hidden tags should be removed
				List<Tag> tagList = new ArrayList<>();
				for (Tag t: instutute.getTags()) {
					if (t.getName().compareTo(identifyTag) != 0) {
						tagList.add(t);
						tagString = tagString + t.getName() + " ";
					}
				}
				
				// StringUtils.join seems to fail - also when using different versions
				//String tagString = StringUtils.join(tagList, " ");
					
				final CmsSelectWidgetOption cslstyleOption = new CmsSelectWidgetOption(tagString.trim(), false, instutute.getResource().getTitle());	
				instituteOptions.add(cslstyleOption);
			}
			return instituteOptions;
			
			
		} catch (final IllegalArgumentException e) {
			log.error("auth error?", e);
			//return "Check your username and api key";
		} catch (final Exception e) {
			log.error("error while rendering publications", e);
			//return "Error while rendering posts";
		}
		
		return instituteOptions;
	}
	
	
	
	/**
	 * @param apiUserName
	 * @param apiKey
	 * @param grouping 
	 * @param groupingName
	 * @param tags
	 * @param search
	 * @param numberOfPublications 
	 * @param removeDuplicates 
	 * @return
	 */
	private static List<Post<Bookmark>> getBookmarks(String apiUrl, final String apiUserName, final String apiKey, GroupingEntity grouping, String groupingName, List<String> tags, String search, int numberOfPublications, boolean removeDuplicates) {
		if (!present(apiUrl)) {
			apiUrl = RestLogicFactory.BIBSONOMY_API_URL;
		}
		final RestLogicFactory factory = new RestLogicFactory(apiUrl);
		final LogicInterface logic = factory.getLogicAccess(apiUserName, apiKey);
		
		final List<Post<Bookmark>> allBookmarks = new LinkedList<Post<Bookmark>>();
		
		int start = 0;
		List<Post<Bookmark>> currentList;
		int retrievedPostListSize;
		int numberOfPublicationsToRetrieve;
		
		// TODO: maybe very inefficient when using removeDuplicates
		do {
			numberOfPublicationsToRetrieve = Math.min(numberOfPublications, MAX_QUERY_SIZE);
			currentList = logic.getPosts(Bookmark.class, grouping, groupingName, tags, null, search, null, null, null, null, null, start, start + numberOfPublicationsToRetrieve);
			retrievedPostListSize = currentList.size();
			
			// not supported for Bookmarks
			//			if (removeDuplicates) {
//				BibTexUtils.removeDuplicates(currentList);
//			}
			allBookmarks.addAll(currentList);
			start += numberOfPublicationsToRetrieve;
			numberOfPublications -= currentList.size(); // currentList.size != retrievedPostListSize
		} while (retrievedPostListSize == numberOfPublicationsToRetrieve && numberOfPublications > 0);
		return allBookmarks;
		
		
	}
	
	
	
	
	
	
}
