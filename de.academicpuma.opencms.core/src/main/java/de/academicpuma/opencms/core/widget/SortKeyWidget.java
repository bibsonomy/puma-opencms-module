package de.academicpuma.opencms.core.widget;

import java.util.LinkedList;
import java.util.List;

import org.bibsonomy.common.enums.SortKey;
import org.opencms.widgets.CmsSelectWidget;
import org.opencms.widgets.CmsSelectWidgetOption;
import org.opencms.widgets.I_CmsWidget;

/**
 * widget for selecting {@link SortKey}
 *
 * @author dzo
 */
public class SortKeyWidget extends CmsSelectWidget {
	
private static final List<CmsSelectWidgetOption> CONFIGURATION = new LinkedList<CmsSelectWidgetOption>();
	
	static {
		for (final SortKey sortKey : new SortKey[] { SortKey.NONE, SortKey.AUTHOR, SortKey.ENTRYTYPE, SortKey.TITLE, SortKey.YEAR}) {
			final String sortKeyString = sortKey.toString();
			final CmsSelectWidgetOption userOption = new CmsSelectWidgetOption(sortKeyString, true, sortKeyString.toLowerCase());
			CONFIGURATION.add(userOption);
		}
	}
	/**
	 * default constructor
	 */
	public SortKeyWidget() {
		super(CONFIGURATION);
	}

	/**
	 * @param configuration
	 */
	public SortKeyWidget(String configuration) {
		super(CONFIGURATION);
	}

	/* (non-Javadoc)
	 * @see org.opencms.widgets.I_CmsWidget#newInstance()
	 */
	@Override
	public I_CmsWidget newInstance() {
		return new SortKeyWidget(getConfiguration());
	}
}