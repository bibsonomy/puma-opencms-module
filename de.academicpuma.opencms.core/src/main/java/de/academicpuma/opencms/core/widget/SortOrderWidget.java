package de.academicpuma.opencms.core.widget;

import java.util.LinkedList;
import java.util.List;

import org.bibsonomy.common.enums.SortOrder;
import org.opencms.widgets.CmsSelectWidget;
import org.opencms.widgets.CmsSelectWidgetOption;
import org.opencms.widgets.I_CmsWidget;

/**
 * widget for selecting {@link SortOrder}
 *
 * @author dzo
 */
public class SortOrderWidget extends CmsSelectWidget {
	
	private static final List<CmsSelectWidgetOption> CONFIGURATION = new LinkedList<CmsSelectWidgetOption>();
	
	static {
		final CmsSelectWidgetOption ascOrderOption = new CmsSelectWidgetOption(SortOrder.ASC.toString(), true, "ascending");
		CONFIGURATION.add(ascOrderOption);
		
		final CmsSelectWidgetOption descOrderOption = new CmsSelectWidgetOption(SortOrder.DESC.toString(), true, "descending");
		CONFIGURATION.add(descOrderOption);
	}
	/**
	 * default constructor
	 */
	public SortOrderWidget() {
		super(CONFIGURATION);
	}

	/**
	 * @param configuration
	 */
	public SortOrderWidget(String configuration) {
		super(CONFIGURATION);
	}

	/* (non-Javadoc)
	 * @see org.opencms.widgets.I_CmsWidget#newInstance()
	 */
	@Override
	public I_CmsWidget newInstance() {
		return new SortOrderWidget(getConfiguration());
	}
}