package de.academicpuma.opencms.core.widget;

import org.apache.commons.logging.Log;
import org.opencms.main.CmsLog;
import org.opencms.widgets.CmsSelectWidget;
import org.opencms.widgets.CmsSelectWidgetOption;
import org.opencms.widgets.I_CmsWidget;

import java.util.LinkedList;
import java.util.List;

/**
 * @author dzo
 */
public class SourceSelectWidget extends CmsSelectWidget {
	private static final Log LOG = CmsLog.getLog(SourceSelectWidget.class);

	/** the user source string representation */
	public static final String USER_SOURCE = "USER";
	
	/** the group source string representation */
	public static final String GROUP_SOURCE = "GROUP";
	
	/** the viewable source string representation */
	public static final String VIEWABLE_SOURCE = "VIEWABLE";
	
	private static final List<CmsSelectWidgetOption> CONFIGURATION = new LinkedList<CmsSelectWidgetOption>();
	
	static {
		final CmsSelectWidgetOption userOption = new CmsSelectWidgetOption(USER_SOURCE, true, USER_SOURCE.toLowerCase());
		CONFIGURATION.add(userOption);
		
		final CmsSelectWidgetOption groupOption = new CmsSelectWidgetOption(GROUP_SOURCE, false, GROUP_SOURCE.toLowerCase());
		CONFIGURATION.add(groupOption);
		
		final CmsSelectWidgetOption viewableOption = new CmsSelectWidgetOption(VIEWABLE_SOURCE, false, VIEWABLE_SOURCE.toLowerCase());
		CONFIGURATION.add(viewableOption);
	}
	
	/**
	 * default constructor
	 */
	public SourceSelectWidget() {
		super(CONFIGURATION);

		LOG.trace("SourceSelectWidget() invoked.");
		LOG.trace("super(" + CONFIGURATION + ") invoked.");
	}

	/**
	 * @param configuration
	 */
	public SourceSelectWidget(String configuration) {
		super(CONFIGURATION);
		LOG.trace("SourceSelectWidget(" + configuration + ") invoked. (Explicit configuration ignored.)");
		LOG.trace("super(" + CONFIGURATION + ") invoked.");
	}

	/* (non-Javadoc)
	 * @see org.opencms.widgets.I_CmsWidget#newInstance()
	 */
	@Override
	public I_CmsWidget newInstance() {
		LOG.trace("newInstance() invoked");

		return new SourceSelectWidget(getConfiguration());
	}
}
