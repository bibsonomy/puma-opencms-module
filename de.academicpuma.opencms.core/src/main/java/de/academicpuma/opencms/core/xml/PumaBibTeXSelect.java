package de.academicpuma.opencms.core.xml;

public class PumaBibTeXSelect {
	public static final String ELEMENT_AUTHOR = "Author";
	public static final String ELEMENT_ENTRYTYPE = "EntryType";
	public static final String ELEMENT_TITLE = "Title";
	public static final String ELEMENT_DOI = "DOI";
	public static final String ELEMENT_ISBN = "ISBN";
	public static final String ELEMENT_ISSN = "ISSN";
	public static final String ELEMENT_JOURNAL = "Journal";
	public static final String ELEMENT_VOLUME = "Volume";
	public static final String ELEMENT_YEAR = "Year";
	public static final String ELEMENT_PUBLISHER = "Publisher";
	public static final String ELEMENT_ADRESS = "Adress";
	public static final String ELEMENT_EDITION = "Edition";
	public static final String ELEMENT_INSTITUTION = "Institution";
	public static final String ELEMENT_ORGANISATION = "Organization";
	public static final String ELEMENT_SERIES = "Series";
	public static final String ELEMENT_SCHOOL = "School";
	public static final String ELEMENT_LANGUAGE = "Language";
	public static final String ELEMENT_MISC = "Misc";
}
