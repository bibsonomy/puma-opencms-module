package de.academicpuma.opencms.core.xml;

public class PumaPublicationList {
	/** @deprecated Currently not included in the schema */
	public static final String ELEMENT_INSTITUTE = "Institute";

	public static final String ELEMENT_SOURCE = "Source";
	public static final String ELEMENT_SOURCE_ID = "Source-ID";
	public static final String ELEMENT_TAGS = "Tags";
	public static final String ELEMENT_BIBTEX = "BibTeX-Field";
	public static final String ELEMENT_EXCLUDE_TAGS = "Exclude-Tags";
	public static final String ELEMENT_EXCLUDE_BIBTEX = "Exclude-BibTeX-Field";
	public static final String ELEMENT_SEARCH = "Search";
	public static final String ELEMENT_SORT_KEY = "Sort-By";
	public static final String ELEMENT_SORT_ORDER = "Sort-Order";
	public static final String ELEMENT_SORT_KEY2 = "Sort-By2";
	public static final String ELEMENT_SORT_ORDER2 = "Sort-Order2";
	public static final String ELEMENT_SORT_KEY3 = "Sort-By3";
	public static final String ELEMENT_SORT_ORDER3 = "Sort-Order3";
	public static final String ELEMENT_NUMBER_OF_PUBLICATIONS = "Number-Of-Publications";
	public static final String ELEMENT_GROUP_PUBLICATIONS = "Group";
	public static final String ELEMENT_REMOVE_DUPLICATES = "Remove-Duplicates";
	public static final String ELEMENT_CSL_STYLE = "CSL-Style";
	public static final String ELEMENT_CSL_TEMPLATE = "CSL-Template";
	public static final String ELEMENT_SHOW_ABSTRACT = "Show-Abstract";
	public static final String ELEMENT_SHOW_BIBTEX = "Show-BibTeX";
	public static final String ELEMENT_SHOW_LINK = "Show-Link";
	public static final String ELEMENT_API_USERNAME = "API-Username";
	public static final String ELEMENT_API_KEY = "API-Key";
	public static final String ELEMENT_API_HOST = "API-Host";
}
