package de.academicpuma.opencms.core;

import de.academicpuma.opencms.core.util.CmsJspContentAccessValueWrapperFactory;
import de.academicpuma.opencms.core.util.grouper.PublicationGrouper;

import org.apache.commons.logging.Log;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.SortOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.opencms.i18n.CmsEncoder;
import org.opencms.jsp.util.CmsJspContentAccessValueWrapper;
import org.opencms.main.CmsLog;
import org.opencms.util.CmsFileUtil;
import org.opencms.xml.CmsXmlException;
import org.opencms.xml.content.CmsXmlContent;
import org.opencms.xml.content.CmsXmlContentFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


public class JspCSLRenderBeanParametersTest extends JspCSLRenderBeanParametersUnit {
	private static final Log LOG = CmsLog.getLog(JspCSLRenderBeanParametersTest.class);

	// TODO: AG 2019-02-26 the second and third tests are identical to the first one and should be changed to test the unmarshalling
	static Stream<Arguments> data() {
		return Stream.of(

				// testFilename, apiUrl, apiUserName, apiKey,
				// grouping, groupingName, tags,
				// bibtexField
				// search, numberOfPublications, removeDuplicates,
				// sortKeys, sortOrders,
				// cslTemplate, publicationGrouper,
				// showAbstract, showBibTeX, showLink
				Arguments.of("testBeanParameters01.xml", "test01-api-url", "test01-api-user-name", "test01-api-key",
						GroupingEntity.GROUP, "test01-source-id", Collections.singletonList("test01-tag01"),
						new String[]{"test01-author01", "test01-author02"},
						"test01-search", 101, false,
						Arrays.asList(SortKey.NONE, SortKey.NONE, SortKey.NONE), Arrays.asList(SortOrder.DESC, SortOrder.DESC, SortOrder.DESC),
						Layouts.getCSLTemplate("ieee"), null,
						false, true, true
				),
				Arguments.of("testBeanParameters02.xml", "test02-api-url", "test02-api-user-name", "test02-api-key",
						GroupingEntity.GROUP, "test02-source-id", Collections.singletonList("test02-tag01"),
						new String[]{"test02-author01", "test02-author01"},
						"test02-search", 102, false,
						Arrays.asList(SortKey.NONE, SortKey.NONE, SortKey.NONE), Arrays.asList(SortOrder.DESC, SortOrder.DESC, SortOrder.DESC),
						Layouts.getCSLTemplate("ieee"), null,
						false, true, true
				),
				Arguments.of("testBeanParameters03.xml", "test03-api-url", "test03-api-user-name", "test03-api-key",
						GroupingEntity.GROUP, "test03-source-id", Collections.singletonList("test03-tag01"),
						new String[]{"test03-author01", "test03-author02"},
						"test03-search", 103, false,
						Arrays.asList(SortKey.NONE, SortKey.NONE, SortKey.NONE), Arrays.asList(SortOrder.DESC, SortOrder.DESC, SortOrder.DESC),
						Layouts.getCSLTemplate("ieee"), null,
						false, true, true
				)
		);
	}

	/**
	 * @param testFilename relative filename
	 */
	@ParameterizedTest
	@MethodSource("data")
	void testSerializingOfFiles(String testFilename, String expectedApiUrl, String expectedApiUserName, String expectedApiKey,
	                            GroupingEntity expectedGrouping, String expectedGroupingName, List<String> expectedTags,
	                            String[] expectedBibtexFields,
	                            String expectedSearch, int expectedNumberOfPublications, boolean expectedRemoveDuplicates,
	                            List<SortKey> expectedSortKeys, List<SortOrder> expectedSortOrders,
	                            String expectedCslTemplate, PublicationGrouper expectedPublicationGrouper,
	                            boolean expectedShowAbstract, boolean expectedShowBibTeX, boolean expectedShowLink) throws IOException, CmsXmlException, PumaException {
		String testFilenamePath = REL_PATH + testFilename;

		LOG.info("Testing " + testFilenamePath);
		String pageStr = CmsFileUtil.readFile(testFilenamePath, CmsEncoder.ENCODING_UTF_8);

		CmsXmlContent xmlContent = CmsXmlContentFactory.unmarshal(pageStr, CmsEncoder.ENCODING_UTF_8, resolver);

		Map<String, CmsJspContentAccessValueWrapper> params = CmsJspContentAccessValueWrapperFactory.getValue(xmlContent, Locale.GERMAN);
		JspCSLRenderBean.BeanParameters p = new JspCSLRenderBean.BeanParameters(params);

		assertThat(p.getApiKey()).isEqualTo(expectedApiKey);
		assertThat(p.getApiUrl()).isEqualTo(expectedApiUrl);
		assertThat(p.getApiUsername()).isEqualTo(expectedApiUserName);
		assertThat(p.getCslTemplate()).isEqualTo(expectedCslTemplate);
		assertThat(p.getGrouping()).isEqualTo(expectedGrouping);
		assertThat(p.getGroupingName()).isEqualTo(expectedGroupingName);
		assertThat(p.getNumberOfPublications()).isEqualTo(expectedNumberOfPublications);
		assertThat(p.getPublicationGrouper()).isEqualTo(expectedPublicationGrouper);
		assertThat(p.getRemoveDuplicates()).isEqualTo(expectedRemoveDuplicates);

		// E.g.:
		//  - XPath(BibSonomyTypes / BibSonomyType / BibTeX-Field / Author) = "test01-author01" OR "test01-author02"
		//  - XPath(BibSonomyTypes / BibSonomyType / Search) = "test01-search"
		//
		//  -> "(test01-search) AND (author:\"test01-author01\" OR \"test01-author02\")"
		assertThat(p.getSearch()).contains(expectedSearch);
		for (String expectedBibtexField : expectedBibtexFields) {
			assertThat(p.getSearch()).contains(expectedBibtexField);
		}

		assertThat(p.getShowAbstract()).isEqualTo(expectedShowAbstract);
		assertThat(p.getShowBibTeX()).isEqualTo(expectedShowBibTeX);
		assertThat(p.getShowLink()).isEqualTo(expectedShowLink);
		assertThat(p.getSortKeys()).isEqualTo(expectedSortKeys);
		assertThat(p.getSortOrders()).isEqualTo(expectedSortOrders);
		assertThat(p.getTags()).isEqualTo(expectedTags);
	}

}