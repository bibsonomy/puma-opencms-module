package de.academicpuma.opencms.core;

import org.junit.jupiter.api.BeforeAll;
import org.opencms.i18n.CmsEncoder;
import org.opencms.main.OpenCms;
import org.opencms.util.CmsFileUtil;
import org.opencms.widgets.CmsTextareaWidget;
import org.opencms.xml.CmsXmlContentTypeManager;
import org.opencms.xml.CmsXmlEntityResolver;
import org.opencms.xml.types.CmsXmlBooleanValue;
import org.xml.sax.EntityResolver;

/**
 * Initializes JUnit test classes registering types and widgets required for puma.
 */
public class JspCSLRenderBeanParametersUnit {
	static final String REL_PATH = "de/academicpuma/opencms/core/";
	/**
	 * Required to unmarshal files.
	 */
	static EntityResolver resolver;

	@BeforeAll
	public static void setUp() throws Exception {
		final String PUMA_PUBLICATION_LIST_XSD = "system/modules/de.academicpuma.opencms.core/schemas/PumaPublicationList.xsd";
		final String PUMA_BIBTEX_SELECT_XSD = "system/modules/de.academicpuma.opencms.core/schemas/PumaBibTeXSelect.xsd";
		final String PUMA_PUBLICATION_LIST_XSD_ID = "opencms://system/modules/de.academicpuma.opencms.core/schemas/PumaPublicationList.xsd";
		final String PUMA_BIBTEX_SELECT_XSD_ID = "opencms://system/modules/de.academicpuma.opencms.core/schemas/PumaBibTeXSelect.xsd";

		// Register the required types
		CmsXmlContentTypeManager typeManager = OpenCms.getXmlContentTypeManager();
		typeManager.addContentType(CmsXmlBooleanValue.class);

		// Register widgets to avoid exceptions during OpenCms's unmarshalling of xml contents
		typeManager.addWidget(CmsTextareaWidget.class.getName(), "TextareaWidget", "");
		typeManager.addWidget(org.opencms.widgets.CmsCheckboxWidget.class.getName(), "BooleanWidget", "");

		// Create the XML entity resolver
		resolver = new CmsXmlEntityResolver(null);

		// Cache Puma XSDs
		String pumaPublicationListXsd = CmsFileUtil.readFile(PUMA_PUBLICATION_LIST_XSD, CmsEncoder.ENCODING_UTF_8);
		CmsXmlEntityResolver.cacheSystemId(PUMA_PUBLICATION_LIST_XSD_ID, pumaPublicationListXsd.getBytes());
		String pumaBibtexSelectXsd = CmsFileUtil.readFile(PUMA_BIBTEX_SELECT_XSD, CmsEncoder.ENCODING_UTF_8);
		CmsXmlEntityResolver.cacheSystemId(PUMA_BIBTEX_SELECT_XSD_ID, pumaBibtexSelectXsd.getBytes());
	}
}
