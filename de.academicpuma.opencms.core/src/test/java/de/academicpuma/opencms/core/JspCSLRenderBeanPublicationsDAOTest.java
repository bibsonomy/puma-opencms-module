package de.academicpuma.opencms.core;

import de.academicpuma.opencms.core.util.Posts;

import org.bibsonomy.common.enums.Filter;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.SearchType;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.enums.Order;
import org.bibsonomy.model.logic.LogicInterface;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@ExtendWith(MockitoExtension.class)
public class JspCSLRenderBeanPublicationsDAOTest {

	private static final List<Post<BibTex>> ONE_POST = new ArrayList<>();
	private static final List<Post<BibTex>> FEW_DUPLICATED_POSTS = new ArrayList<>();
	private static final List<Post<BibTex>> MANY_DUPLICATED_POSTS = new ArrayList<>();
	private static final List<Post<BibTex>> MANY_RANDOM_POSTS = new ArrayList<>();
	private static final List<Post<BibTex>> MANY_RANDOM_POSTS_2 = new ArrayList<>();
	private static final List<Post<BibTex>> MANY_RANDOM_POSTS_3 = new ArrayList<>();
	private static final Post<BibTex> EXAMPLE_POST = Posts.generatePublicationPost(2019);
	private static final Post<BibTex> EXAMPLE_POST_2 = Posts.generatePublicationPost(2018);

	static {
		ONE_POST.add(EXAMPLE_POST);
		for (int i = 0; i < Module.MAX_QUERY_SIZE; i++) {
			MANY_DUPLICATED_POSTS.add(EXAMPLE_POST);
			MANY_RANDOM_POSTS.add(Posts.generatePublicationPost(3000 + i));
			MANY_RANDOM_POSTS_2.add(Posts.generatePublicationPost(3000 + Module.MAX_QUERY_SIZE + i));
			MANY_RANDOM_POSTS_3.add(Posts.generatePublicationPost(3000 + Module.MAX_QUERY_SIZE * 2 + i));
		}
		for (int i = 0; i < 3; i++) {
			FEW_DUPLICATED_POSTS.add(EXAMPLE_POST_2);
		}
	}

	@Mock
	private LogicInterface mockedLoginInterface;
	private JspCSLRenderBean.PublicationsDAO publicationsDAO;

	public static List<Post<BibTex>> getOnePost() {
		return new ArrayList<>(ONE_POST);
	}

	/**
	 * 3 identical posts.
	 */
	public static List<Post<BibTex>> getFewDuplicatedPosts() {
		return new ArrayList<>(FEW_DUPLICATED_POSTS);
	}

	/**
	 * {@link Module#MAX_QUERY_SIZE} identical posts.
	 */
	public static List<Post<BibTex>> getManyDuplicatedPosts() {
		return new ArrayList<>(MANY_DUPLICATED_POSTS);
	}

	/**
	 * {@link Module#MAX_QUERY_SIZE} random posts.
	 */
	public static List<Post<BibTex>> getManyRandomPosts() {
		return new ArrayList<>(MANY_RANDOM_POSTS);
	}

	/**
	 * {@link Module#MAX_QUERY_SIZE} random posts (different from {@link #MANY_RANDOM_POSTS}.
	 */
	public static List<Post<BibTex>> getManyRandomPosts2() {
		return new ArrayList<>(MANY_RANDOM_POSTS_2);
	}

	/**
	 * {@link Module#MAX_QUERY_SIZE} random posts (different from {@link #MANY_RANDOM_POSTS}
	 * and {@link #MANY_RANDOM_POSTS_2}.
	 */
	public static List<Post<BibTex>> getManyRandomPosts3() {
		return new ArrayList<>(MANY_RANDOM_POSTS_3);
	}

	@BeforeEach
	public void setUpMocks() {
		publicationsDAO = new JspCSLRenderBean.PublicationsDAO() {
			{
				setLogic(mockedLoginInterface);
			}
		};
	}

	@Test
	public void testUsageOfLogicInterface() {
		// given
		final GroupingEntity GROUP_ENTITY = GroupingEntity.GROUP;
		final String GROUPING_NAME = "testUsageOfLogicInterface_GroupName";
		final List<String> TAGS = Collections.singletonList("testUsageOfLogicInterface_TAGS");
		final String SEARCH = "testUsageOfLogicInterface_GroupName_Search";
		final int NUMBER_OF_PUBS = 101;
		final boolean REMOVE_DUPLICATES = false;
		given(mockedLoginInterface.getPosts(eq(BibTex.class),
				ArgumentMatchers.<GroupingEntity>any(), anyString(),
				ArgumentMatchers.<String>anyList(),
				ArgumentMatchers.<String>isNull(),
				anyString(), ArgumentMatchers.<SearchType>isNull(),
				ArgumentMatchers.<Set<Filter>>isNull(), ArgumentMatchers.<Order>isNull(),
				ArgumentMatchers.<Date>isNull(), ArgumentMatchers.<Date>isNull(),
				anyInt(), anyInt()))
				.willReturn(getOnePost());

		// when
		publicationsDAO.getPublications(GROUP_ENTITY, GROUPING_NAME, TAGS, SEARCH, NUMBER_OF_PUBS, REMOVE_DUPLICATES);

		// then
		then(mockedLoginInterface).should().getPosts(BibTex.class,
				GroupingEntity.GROUP, GROUPING_NAME,
				TAGS,
				null,
				SEARCH, null,
				null, null,
				null, null,
				0, NUMBER_OF_PUBS);
	}

	@Test
	public void shouldNotFilterDuplicatedPublications() {
		// given
		final GroupingEntity GROUP_ENTITY = GroupingEntity.GROUP;
		final String GROUPING_NAME = "shouldNotFilterDuplicatedPublications_GroupName";
		final List<String> TAGS = Collections.singletonList("shouldNotFilterDuplicatedPublications_TAGS");
		final String SEARCH = "shouldNotFilterDuplicatedPublications_GroupName_Search";
		final int NUMBER_OF_PUBS = 101;
		final boolean REMOVE_DUPLICATES = false;
		given(mockedLoginInterface.getPosts(eq(BibTex.class),
				ArgumentMatchers.<GroupingEntity>any(), anyString(),
				ArgumentMatchers.<String>anyList(),
				ArgumentMatchers.<String>isNull(),
				anyString(), ArgumentMatchers.<SearchType>isNull(),
				ArgumentMatchers.<Set<Filter>>isNull(), ArgumentMatchers.<Order>isNull(),
				ArgumentMatchers.<Date>isNull(), ArgumentMatchers.<Date>isNull(),
				anyInt(), anyInt()))
				.willReturn(getManyDuplicatedPosts());

		// when
		List<Post<BibTex>> publications = publicationsDAO.getPublications(GROUP_ENTITY, GROUPING_NAME, TAGS, SEARCH, NUMBER_OF_PUBS, REMOVE_DUPLICATES);

		// then
		assertThat(publications.size()).isEqualTo(getManyDuplicatedPosts().size());
		assertThat(publications.get(0)).isEqualTo(EXAMPLE_POST);
	}

	@Test
	@Disabled("Must fix the FIXME bug in PublicationsDAO#getPublications")
	public void shouldFilterDuplicatedPublications() {
		final GroupingEntity GROUP_ENTITY = GroupingEntity.GROUP;
		final String GROUPING_NAME = "shouldFilterDuplicatedPublications_GroupName";
		final List<String> TAGS = Collections.singletonList("shouldFilterDuplicatedPublications_TAGS");
		final String SEARCH = "shouldFilterDuplicatedPublications_GroupName_Search";
		final int NUMBER_OF_PUBS = 3101;
		final boolean REMOVE_DUPLICATES = true;

		// given
		given(mockedLoginInterface.getPosts(eq(BibTex.class),
				ArgumentMatchers.<GroupingEntity>any(), anyString(),
				ArgumentMatchers.<String>anyList(),
				ArgumentMatchers.<String>isNull(),
				anyString(), ArgumentMatchers.<SearchType>isNull(),
				ArgumentMatchers.<Set<Filter>>isNull(), ArgumentMatchers.<Order>isNull(),
				ArgumentMatchers.<Date>isNull(), ArgumentMatchers.<Date>isNull(),
				anyInt(), anyInt()))
				.willReturn(getManyDuplicatedPosts())
				.willReturn(getManyDuplicatedPosts())
				.willReturn(getManyDuplicatedPosts())
				.willReturn(getManyDuplicatedPosts())
				.willReturn(getOnePost());

		// when
		List<Post<BibTex>> publications = publicationsDAO.getPublications(GROUP_ENTITY, GROUPING_NAME, TAGS, SEARCH, NUMBER_OF_PUBS, REMOVE_DUPLICATES);

		// then
		assertThat(publications.size()).isEqualTo(1);
		assertThat(publications.get(0)).isEqualTo(EXAMPLE_POST);
	}


	/**
	 * {@link JspCSLRenderBean.PublicationsDAO#getPublications(GroupingEntity, String, List, String, int, boolean)}
	 * should return 2 items when requesting posts, given that the backend answers 4 times with the same replicated
	 * publication and 1 time with a different post.
	 */
	@Test
	@Disabled("Must fix the FIXME bug in PublicationsDAO#getPublications")
	public void shouldFilterFewDuplicatedPublications() {
		final GroupingEntity GROUP_ENTITY = GroupingEntity.GROUP;
		final String GROUPING_NAME = "shouldFilterFewDuplicatedPublications_GroupName";
		final List<String> TAGS = Collections.singletonList("shouldFilterFewDuplicatedPublications_TAGS");
		final String SEARCH = "shouldFilterFewDuplicatedPublications_GroupName_Search";
		final int NUMBER_OF_PUBS = 3;
		final boolean REMOVE_DUPLICATES = true;


		// given
		given(mockedLoginInterface.getPosts(eq(BibTex.class),
				ArgumentMatchers.<GroupingEntity>any(), anyString(),
				ArgumentMatchers.<String>anyList(),
				ArgumentMatchers.<String>isNull(),
				anyString(), ArgumentMatchers.<SearchType>isNull(),
				ArgumentMatchers.<Set<Filter>>isNull(), ArgumentMatchers.<Order>isNull(),
				ArgumentMatchers.<Date>isNull(), ArgumentMatchers.<Date>isNull(),
				anyInt(), anyInt()))
				.willReturn(getFewDuplicatedPosts())
				.willReturn(getFewDuplicatedPosts())
				.willReturn(getFewDuplicatedPosts())
				.willReturn(getFewDuplicatedPosts())
				.willReturn(getOnePost());

		// when
		List<Post<BibTex>> publications = publicationsDAO.getPublications(GROUP_ENTITY, GROUPING_NAME, TAGS, SEARCH, NUMBER_OF_PUBS, REMOVE_DUPLICATES);

		// then
		assertThat(publications.size()).isEqualTo(2);
		assertThat(publications).contains(EXAMPLE_POST);
		assertThat(publications).contains(EXAMPLE_POST_2);
	}

	@Test
	public void shouldReturnEnoughPublications() {
		final GroupingEntity GROUP_ENTITY = GroupingEntity.GROUP;
		final String GROUPING_NAME = "testUsageOfLogicInterface_GroupName";
		final List<String> TAGS = Collections.singletonList("testUsageOfLogicInterface_TAGS");
		final String SEARCH = "testUsageOfLogicInterface_GroupName_Search";
		final int NUMBER_OF_PUBS = 3001;
		final boolean REMOVE_DUPLICATES = true;

		// given
		given(mockedLoginInterface.getPosts(eq(BibTex.class),
				ArgumentMatchers.<GroupingEntity>any(), anyString(),
				ArgumentMatchers.<String>anyList(),
				ArgumentMatchers.<String>isNull(),
				anyString(), ArgumentMatchers.<SearchType>isNull(),
				ArgumentMatchers.<Set<Filter>>isNull(), ArgumentMatchers.<Order>isNull(),
				ArgumentMatchers.<Date>isNull(), ArgumentMatchers.<Date>isNull(),
				anyInt(), anyInt()))
				.willReturn(getManyDuplicatedPosts())
				.willReturn(getManyRandomPosts())
				.willReturn(getManyRandomPosts2())
				.willReturn(getManyRandomPosts3());

		// when
		List<Post<BibTex>> publications = publicationsDAO.getPublications(GROUP_ENTITY, GROUPING_NAME, TAGS, SEARCH, NUMBER_OF_PUBS, REMOVE_DUPLICATES);

		// then
		assertThat(publications.size()).isEqualTo(NUMBER_OF_PUBS);
		assertThat(publications).contains(EXAMPLE_POST);
		assertThat(publications).contains(getManyRandomPosts().get(0));
		assertThat(publications).contains(getManyRandomPosts().get(999));
		assertThat(publications).contains(getManyRandomPosts2().get(0));
		assertThat(publications).contains(getManyRandomPosts3().get(0));
	}
}