package de.academicpuma.opencms.core;

import de.academicpuma.opencms.core.util.Posts;

import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.SortOrder;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.util.PersonNameParser;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class JspCSLRenderBeanPublicationsHtmlFormatterTest {

	private static final Post<BibTex> POST_AVA;
	private static final Post<BibTex> POST_EMMA;
	private static final Post<BibTex> POST_ISABELLA;
	private static final Post<BibTex> POST_OLIVIA;
	private static final Post<BibTex> POST_SOPHIA;
	private static final Post<BibTex> POST_AVA_AND_EMMA;
	private static final Post<BibTex> POST_AVA_AND_EMMA_AND_ISABELLA;
	private static final Post<BibTex> POST_AVA_AND_EMMA_AND_ISABELLA_AND_OLIVIA;
	private static final Post<BibTex> POST_AVA_WITH_ATTACHMENT;
	private static final Post<BibTex> POST_WITH_MANY_ATTACHMENTS;
	private static final Post<BibTex> POST_2017_NOV_11;
	private static final Post<BibTex> POST_2017_JUN_28;
	private static final Post<BibTex> POST_2002_JUN_25;
	private static final List<Post<BibTex>> ONE_POST;

	private static final String FILENAME_ATTACHMENT_TXT = "Attachment.txt";
	private static final String[] FILENAMES_ATTACHMENTS_TXT = {"Attachment_01.txt", "Attachment_02.txt", "Attachment_03.txt"};


	static {
		try {
			POST_AVA = Posts.generatePublicationPost(2019, PersonNameParser.parse("Smith, Ava"));
			POST_EMMA = Posts.generatePublicationPost(2019, PersonNameParser.parse("Smith, Emma"));
			POST_ISABELLA = Posts.generatePublicationPost(2019, PersonNameParser.parse("Smith, Isabella"));
			POST_OLIVIA = Posts.generatePublicationPost(2019, PersonNameParser.parse("Smith, Olivia"));
			POST_SOPHIA = Posts.generatePublicationPost(2019, PersonNameParser.parse("Smith, Sophia"));
			POST_AVA_AND_EMMA = Posts.generatePublicationPost(2019, PersonNameParser.parse("Smith, Ava and Smith, Emma"));
			POST_AVA_AND_EMMA_AND_ISABELLA = Posts.generatePublicationPost(2019, PersonNameParser.parse("Smith, Ava and Smith, Emma and Smith, Isabella"));
			POST_AVA_AND_EMMA_AND_ISABELLA_AND_OLIVIA = Posts.generatePublicationPost(2019, PersonNameParser.parse("Smith, Ava and Smith, Emma and Smith, Isabella and Smith, Olivia"));

			POST_2017_NOV_11 = Posts.generatePublicationPost("2017", "11", "21", "2017-11-21-https://doi.org/10.1186/s13068-017-0969-8");
			POST_2017_JUN_28 = Posts.generatePublicationPost("2017", "6", "28", "2017-06-28-https://doi.org/10.3389/fmicb.2017.01195");
			POST_2002_JUN_25 = Posts.generatePublicationPost("2002", "6", "5", "2002-06-25-https://doi.org/10.1007/s00449-002-0279-8");

			{
				POST_AVA_WITH_ATTACHMENT = Posts.generatePublicationPost(2019, PersonNameParser.parse("Smith, Ava"));
				List<Document> attachments = new ArrayList<>();
				Document d = new Document();
				d.setFileName(FILENAME_ATTACHMENT_TXT);
				attachments.add(d);
				POST_AVA_WITH_ATTACHMENT.getResource().setDocuments(attachments);
			}

			{
				POST_WITH_MANY_ATTACHMENTS = Posts.generatePublicationPost(2019);
				List<Document> attachments = new ArrayList<>();
				for (String filename : FILENAMES_ATTACHMENTS_TXT) {
					Document d = new Document();
					d.setFileName(filename);
				}
				POST_WITH_MANY_ATTACHMENTS.getResource().setDocuments(attachments);
			}
		} catch (PersonNameParser.PersonListParserException e) {
			throw new AssertionError("Not expected exception when creating PersonName: " + e.getMessage(), e);
		}
		ONE_POST = Collections.singletonList(POST_EMMA);
	}

	@Test
	public void shouldSortEmptyList() {
		// given
		final ArrayList<Post<BibTex>> posts = new ArrayList<>();
		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);
		final List<org.bibsonomy.common.enums.SortKey> sortKeys = new ArrayList<>();
		final List<SortOrder> sortOrders = new ArrayList<>();

		// when
		formatter.sort(sortKeys, sortOrders);

		// then
		assertThat(posts.size()).isEqualTo(0);
	}

	@Test
	public void shouldSortOnePost() {
		// given
		final ArrayList<Post<BibTex>> posts = new ArrayList<>(ONE_POST);
		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);
		final List<org.bibsonomy.common.enums.SortKey> sortKeys = new ArrayList<>();
		final List<SortOrder> sortOrders = new ArrayList<>();

		// when
		formatter.sort(sortKeys, sortOrders);

		// then
		assertThat(posts.size()).isEqualTo(1);
		assertThat(posts).contains(ONE_POST.get(0));
	}

	@Test
	public void shouldSortByAuthors() {
		// given
		final ArrayList<Post<BibTex>> sortedPosts = new ArrayList<>();
		sortedPosts.add(POST_AVA);
		sortedPosts.add(POST_EMMA);
		sortedPosts.add(POST_ISABELLA);
		sortedPosts.add(POST_OLIVIA);
		sortedPosts.add(POST_SOPHIA);

		final ArrayList<Post<BibTex>> unsortedPosts = new ArrayList<>();
		unsortedPosts.add(POST_EMMA);
		unsortedPosts.add(POST_OLIVIA);
		unsortedPosts.add(POST_AVA);
		unsortedPosts.add(POST_ISABELLA);
		unsortedPosts.add(POST_SOPHIA);
		ArrayList<Post<BibTex>> posts = new ArrayList<>(unsortedPosts);
		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		final List<org.bibsonomy.common.enums.SortKey> sortKeys = new ArrayList<>();
		sortKeys.add(SortKey.AUTHOR);
		final List<SortOrder> sortOrders = new ArrayList<>();

		// when
		formatter.sort(sortKeys, sortOrders);

		// then
		assertThat(posts.size()).isEqualTo(unsortedPosts.size());

		assertThat(posts).isEqualTo(sortedPosts);
	}

	@Test
	public void shouldReverseSortByAuthors() {
		// given
		final ArrayList<Post<BibTex>> sortedPosts = new ArrayList<>();
		sortedPosts.add(POST_SOPHIA);
		sortedPosts.add(POST_OLIVIA);
		sortedPosts.add(POST_ISABELLA);
		sortedPosts.add(POST_EMMA);
		sortedPosts.add(POST_AVA);

		final ArrayList<Post<BibTex>> unsortedPosts = new ArrayList<>();
		unsortedPosts.add(POST_EMMA);
		unsortedPosts.add(POST_OLIVIA);
		unsortedPosts.add(POST_AVA);
		unsortedPosts.add(POST_ISABELLA);
		unsortedPosts.add(POST_SOPHIA);
		ArrayList<Post<BibTex>> posts = new ArrayList<>(unsortedPosts);
		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		final List<org.bibsonomy.common.enums.SortKey> sortKeys = new ArrayList<>();
		sortKeys.add(SortKey.AUTHOR);
		final List<SortOrder> sortOrders = new ArrayList<>();
		sortOrders.add(SortOrder.DESC);

		// when
		formatter.sort(sortKeys, sortOrders);

		// then
		assertThat(posts.size()).isEqualTo(unsortedPosts.size());

		assertThat(posts).isEqualTo(sortedPosts);
	}

	@Test
	@Disabled("Must fix bug in bibsonomy-model-3.9.0: compareAuthor only compares first person in the list")
	public void shouldSortByAuthorsFromFewerToMore() {
		// given
		final ArrayList<Post<BibTex>> sortedPosts = new ArrayList<>();
		sortedPosts.add(POST_AVA);
		sortedPosts.add(POST_AVA_AND_EMMA);
		sortedPosts.add(POST_AVA_AND_EMMA_AND_ISABELLA);
		sortedPosts.add(POST_AVA_AND_EMMA_AND_ISABELLA_AND_OLIVIA);
		sortedPosts.add(POST_EMMA);

		final ArrayList<Post<BibTex>> unsortedPosts = new ArrayList<>();
		unsortedPosts.add(POST_EMMA);
		unsortedPosts.add(POST_AVA);
		unsortedPosts.add(POST_AVA_AND_EMMA_AND_ISABELLA_AND_OLIVIA);
		unsortedPosts.add(POST_AVA_AND_EMMA_AND_ISABELLA);
		unsortedPosts.add(POST_AVA_AND_EMMA);
		ArrayList<Post<BibTex>> posts = new ArrayList<>(unsortedPosts);
		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		final List<org.bibsonomy.common.enums.SortKey> sortKeys = new ArrayList<>();
		sortKeys.add(SortKey.AUTHOR);
		final List<SortOrder> sortOrders = new ArrayList<>();

		// when
		formatter.sort(sortKeys, sortOrders);

		// then
		assertThat(posts.size()).isEqualTo(unsortedPosts.size());
		assertThat(posts).isEqualTo(sortedPosts);
	}

	@Test
	void shouldSortAscendingByYear() {
		// given
		final List<Post<BibTex>> posts = Arrays.asList(POST_2017_JUN_28, POST_2002_JUN_25);
		final List<Post<BibTex>> ascendingList = Arrays.asList(POST_2002_JUN_25, POST_2017_JUN_28);

		final List<org.bibsonomy.common.enums.SortKey> sortKeys = new ArrayList<>();
		sortKeys.add(SortKey.YEAR);
		final List<SortOrder> sortOrders = new ArrayList<>();
		sortOrders.add(SortOrder.ASC);

		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		// when
		formatter.sort(sortKeys, sortOrders);

		// then
		assertThat(posts).as("Posts were not sorted ascendent").isEqualTo(ascendingList);
	}

	@Test
	void shouldSortAscendingByYearAndMonth() {
		// given
		final List<Post<BibTex>> posts = Arrays.asList(POST_2017_NOV_11, POST_2017_JUN_28);
		final List<Post<BibTex>> ascendingList = Arrays.asList(POST_2017_JUN_28, POST_2017_NOV_11);

		final List<org.bibsonomy.common.enums.SortKey> sortKeys = new ArrayList<>();
		sortKeys.add(SortKey.YEAR);
		sortKeys.add(SortKey.MONTH);
		final List<SortOrder> sortOrders = new ArrayList<>();
		sortOrders.add(SortOrder.ASC);
		sortOrders.add(SortOrder.ASC);

		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		// when
		formatter.sort(sortKeys, sortOrders);

		// then
		assertThat(posts).as("Posts were not sorted ascendent").isEqualTo(ascendingList);
	}

	@Test
	void shouldSortDescendingByYear() {
		// given
		final List<Post<BibTex>> posts = Arrays.asList(POST_2002_JUN_25, POST_2017_JUN_28);
		final List<Post<BibTex>> descendingList = Arrays.asList(POST_2017_JUN_28, POST_2002_JUN_25);

		final List<org.bibsonomy.common.enums.SortKey> sortKeys = new ArrayList<>();
		sortKeys.add(SortKey.YEAR);
		final List<SortOrder> sortOrders = new ArrayList<>();
		sortOrders.add(SortOrder.DESC);

		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		// when
		formatter.sort(sortKeys, sortOrders);

		// then
		assertThat(posts).as("Posts were not sorted descendent").isEqualTo(descendingList);
	}

	@Test
	void shouldSortDescendingByYearAndMonth() {
		// given
		final List<Post<BibTex>> posts = Arrays.asList(POST_2017_JUN_28, POST_2017_NOV_11);
		final List<Post<BibTex>> descendingList = Arrays.asList(POST_2017_NOV_11, POST_2017_JUN_28);

		final List<org.bibsonomy.common.enums.SortKey> sortKeys = new ArrayList<>();
		sortKeys.add(SortKey.YEAR);
		sortKeys.add(SortKey.MONTH);
		final List<SortOrder> sortOrders = new ArrayList<>();
		sortOrders.add(SortOrder.DESC);
		sortOrders.add(SortOrder.DESC);

		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		// when
		formatter.sort(sortKeys, sortOrders);

		// then
		assertThat(posts).as("Posts were not sorted descendent").isEqualTo(descendingList);
	}

	@Test
	public void shouldNotRenderLinkToAttachments() throws IOException {
		// given
		final ArrayList<Post<BibTex>> posts = new ArrayList<>();
		posts.add(POST_AVA_WITH_ATTACHMENT);
		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		// when
		String formattedPosts = formatter.format(CSLStyles.IEEE.getFilename(), null, false, false, false);

		// then
		assertThat(formattedPosts).doesNotContain(FILENAME_ATTACHMENT_TXT);
	}

	@Test
	@Disabled("NYI")
	public void shouldRenderLinkToAttachments() throws IOException {
		// given
		final ArrayList<Post<BibTex>> posts = new ArrayList<>();
		posts.add(POST_AVA_WITH_ATTACHMENT);
		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		// when
		String formattedPosts = formatter.format(CSLStyles.IEEE.getFilename(), null, false, false, false, true);

		// then
		assertThat(formattedPosts).contains(FILENAME_ATTACHMENT_TXT);
	}

	@Test
	@Disabled("NYI")
	public void shouldRenderLinkToManyAttachments() throws IOException {
		// given
		final ArrayList<Post<BibTex>> posts = new ArrayList<>();
		posts.add(POST_WITH_MANY_ATTACHMENTS);
		JspCSLRenderBean.PublicationsHtmlFormatter formatter = new JspCSLRenderBean.PublicationsHtmlFormatter(posts);

		// when
		String formattedPosts = formatter.format(CSLStyles.IEEE.getFilename(), null, false, false, false, true);

		// then
		for (String filename : FILENAMES_ATTACHMENTS_TXT) {
			assertThat(formattedPosts).contains(filename);
		}
	}

}