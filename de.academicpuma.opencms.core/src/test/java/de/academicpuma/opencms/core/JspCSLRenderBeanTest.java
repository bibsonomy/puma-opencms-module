package de.academicpuma.opencms.core;

import de.academicpuma.opencms.core.util.Posts;
import de.academicpuma.opencms.core.util.grouper.PublicationGrouper;
import de.academicpuma.opencms.core.util.grouper.YearGrouper;
import de.undercouch.citeproc.BibliographyFileReader;
import de.undercouch.citeproc.CSL;
import de.undercouch.citeproc.ItemDataProvider;
import de.undercouch.citeproc.csl.CSLItemData;
import de.undercouch.citeproc.csl.CSLItemDataBuilder;
import de.undercouch.citeproc.csl.CSLNameBuilder;
import de.undercouch.citeproc.csl.CSLType;

import org.apache.commons.logging.Log;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.opencms.main.CmsLog;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * tests for {@link JspCSLRenderBean}
 * 
 * @author dzo
 * @author agr
 */
class JspCSLRenderBeanTest {
	private static final Log LOG = CmsLog.getLog(JspCSLRenderBeanTest.class);
	private final static String CSL_TEMPLATE = Layouts.getCSLTemplate(CSLStyles.HARVARD.getFilename());

	/**
	 * tests {@link JspCSLRenderBean#renderGroupedCSL(List, String, PublicationGrouper, boolean, boolean, boolean)}
	 */
	@Test
	void testRenderGroupedCSL() throws Exception {
		final List<Post<BibTex>> posts = new LinkedList<>();

		posts.add(Posts.generatePublicationPost(2006));
		posts.add(Posts.generatePublicationPost(2007));
		
		final String renderedHTML = JspCSLRenderBean.renderGroupedCSL(posts, CSL_TEMPLATE, new YearGrouper(), false, false, false);

		final String EXPECTED = "<li><h3 class=\"group\">2006</h3><ol><li><div class=\"publication\">  <div class=\"csl-entry\">Doe, John (2006): „test“. In.:</div>\n" +
				"</div></li></ol></li><li><h3 class=\"group\">2007</h3><ol><li><div class=\"publication\">  <div class=\"csl-entry\">Doe, John (2007): „test“. In.:</div>\n" +
				"</div></li></ol></li>";

		assertEquals(EXPECTED, renderedHTML);
	}

	/**
	 * Exercise bib rendering, to verify that no mem is being leaked. (There was a mem-leak in {@code citeproc-java}.)
	 * <p>
	 * To try to fail faster, run with:
	 * <pre>{@code mvn -DargLine="-Xmx64m -XX:MaxMetaspaceSize=40m" -Dtest=de/academicpuma/opencms/core/JspCSLRenderBeanTest#testMemoryLeaks test}</pre>
	 */
	@Test
	void renderGroupedCSLShouldNotLeakMemory() throws IOException {
		final List<Post<BibTex>> posts = Collections.singletonList(Posts.generatePublicationPost(2006));
		int i = 0;
		while (i<1000) {
			i++;
			final String renderedHTML = JspCSLRenderBean.renderGroupedCSL(posts, CSL_TEMPLATE, new YearGrouper(), false, false, false);
			if (i == 1 || i%100 == 0) {
				System.out.println("iteration " + i + ". Html: " + renderedHTML);
			}
		}
	}

	@Test
	void makeAdhocBibliographyShouldNotLeakMemoryWhenRenderingSameItem() throws IOException {
		CSLItemData item;
		item = new CSLItemDataBuilder()
				.type(CSLType.WEBPAGE)
				.title("citeproc-java: A Citation Style Language (CSL) processor for Java")
				.author("Michel", "Krämer")
				.issued(2016, 11, 20)
				.URL("http://michel-kraemer.github.io/citeproc-java/")
				.accessed(2018, 4, 12)
				.build();

		for (int i=1; i <= 3000; i++) {
			if (i == 1 || i % 100 == 0) {
				LOG.info("entry " + i + ":" + CSL.makeAdhocBibliography("ieee", item).makeString());
			}
		}
	}

	@Test
	@Disabled("Not working yet: citeproc-java must be patched first.")
	void shouldRenderABookCorrectly() throws IOException {
		final CSLItemData item;
		try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("de/academicpuma/opencms/core/util/book_sample01.bib")) {
			BibliographyFileReader reader = new BibliographyFileReader();
			ItemDataProvider provider = reader.readBibliographyFile(is, BibliographyFileReader.FileFormat.BIBTEX);
			System.out.println("ids: " + Arrays.toString(provider.getIds()));
			item = provider.retrieveItem(provider.getIds()[0]);
		}

		final String expected =
				"<div class=\"csl-bib-body\">\n" +
				"  <div class=\"csl-entry\">Mustermann, M. (1999). <i>Buchtitel1</i> (Auflage). Adresse: Verlag.</div>\n" +
				"</div>\n";

		assertEquals(expected, CSL.makeAdhocBibliography("apa", item).makeString());
	}

	@Test
	void shouldRenderAnchorsForDoi() throws IOException {
		CSLItemData item;
		item = new CSLItemDataBuilder()
				.type(CSLType.THESIS)
				.title("Properties of expanding universes")
				.author(new CSLNameBuilder().literal("Hawking, Stephen").build())
				.issued(1966, 3, 15)
				.DOI("10.17863/CAM.11283")
				.publisher("University of Cambridge")
				.build();

		final String EXPECTED = "<div class=\"csl-bib-body\">\n" +
				"  <div class=\"csl-entry\">Hawking, Stephen (1966) <i>Properties of expanding universes</i>. University of Cambridge. doi: <a href=\"https://doi.org/10.17863/CAM.11283\">10.17863/CAM.11283</a>.</div>\n" +
				"</div>";
		assertEquals(EXPECTED, CSLWithLinks.makeAdhocBibliography("harvard-cite-them-right", item).makeString());
	}

	@Test
	void shouldRenderAnchorsForUrl() throws IOException {
		CSLItemData item;
		item = new CSLItemDataBuilder()
				.type(CSLType.THESIS)
				.title("Properties of expanding universes")
				.author(new CSLNameBuilder().literal("Hawking, Stephen").build())
				.issued(1966, 3, 15)
				.URL("https://www.repository.cam.ac.uk/handle/1810/251038")
				.publisher("University of Cambridge")
				.build();

		final String EXPECTED = "<div class=\"csl-bib-body\">\n" +
				"  <div class=\"csl-entry\">Hawking, Stephen (1966) <i>Properties of expanding universes</i>. University of Cambridge. Available at: <a href=\"https://www.repository.cam.ac.uk/handle/1810/251038\">https://www.repository.cam.ac.uk/handle/1810/251038</a>.</div>\n" +
				"</div>";
		assertEquals(EXPECTED, CSLWithLinks.makeAdhocBibliography("harvard-cite-them-right", item).makeString());
	}
}
