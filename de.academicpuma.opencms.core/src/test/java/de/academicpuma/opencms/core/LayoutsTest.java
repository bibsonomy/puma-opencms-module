package de.academicpuma.opencms.core;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * tests for {@link Module}
 * 
 * @author dzo
 */
public class LayoutsTest {
	
	/**
	 * tests {@link Layouts#getCSLTemplate(String)}
	 */
	@Test
	public void testGetCSLTemplate() {
		final String ieee = Layouts.getCSLTemplate("ieee");
		assertNotNull(ieee);
		assertThat(ieee).startsWith("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		assertThat(ieee).contains("<title>IEEE</title>");
	}
}
