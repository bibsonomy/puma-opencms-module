package de.academicpuma.opencms.core.util;

import de.undercouch.citeproc.BibliographyFileReader;
import de.undercouch.citeproc.CSL;
import de.undercouch.citeproc.DefaultAbbreviationProvider;
import de.undercouch.citeproc.ItemDataProvider;
import de.undercouch.citeproc.ListItemDataProvider;
import de.undercouch.citeproc.bibtex.BibTeXConverter;
import de.undercouch.citeproc.bibtex.BibTeXItemDataProvider;
import de.undercouch.citeproc.csl.CSLDateBuilder;
import de.undercouch.citeproc.csl.CSLItemData;
import de.undercouch.citeproc.csl.CSLItemDataBuilder;
import de.undercouch.citeproc.csl.CSLNameBuilder;
import de.undercouch.citeproc.csl.CSLType;
import de.undercouch.citeproc.helper.json.JsonLexer;
import de.undercouch.citeproc.helper.json.JsonParser;
import de.undercouch.citeproc.output.Bibliography;

import org.apache.tools.ant.filters.StringInputStream;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.User;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.model.util.PersonNameParser;
import org.bibsonomy.rest.renderer.UrlRenderer;
import org.bibsonomy.rest.renderer.impl.json.JSONRenderer;
import org.bibsonomy.rest.renderer.impl.xml.XMLRenderer;
import org.jbibtex.BibTeXDatabase;
import org.jbibtex.ParseException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * tests for {@link CSLUtils}
 *
 * @author dzo
 */
class CSLUtilsTest {

	/**
	 * tests {@link CSLUtils#cleanBib(String)}
	 */
	@Test
	void cleanBib() throws IOException {
		final Bibliography bibliography = CSL.makeAdhocBibliography("ieee", "html", createSamplePublication());
		final String ieeeEntry = bibliography.getEntries()[0];
		
		assertEquals("J. Doe, “Test.” 2010.", CSLUtils.cleanBib(ieeeEntry));
		
		final Bibliography bibliography2 = CSL.makeAdhocBibliography("vancouver", "html", createSamplePublication());
		final String vancouver = bibliography2.getEntries()[0];
		
		assertEquals("Doe J. Test. 2010.", CSLUtils.cleanBib(vancouver));
	}
	
	/**
	 * tests {@link CSLUtils#cleanBib(String)}
	 * with newlines
	 */
	@Test
	void testCleanBibDotAll() {
		final String toClean = "  <div class=\"csl-entry\">\n" + 
				"    <div class=\"csl-left-margin\">[3]</div><div class=\"csl-right-inline\">S. Doerfel, D. Zoller, P. Singer, T. Niebler, A. Hotho, and M. Strohmaier, “Evaluating Assumptions about Social Tagging - A Study of User Behavior\n" + 
				"&#160; &#160; &#160; &#160; &#160; &#160; &#160;  in BibSonomy,” in <i>Proceedings of the 16th LWA Workshops: KDML, IR and FGWM, Aachen,&#160; &#160; &#160; &#160; &#160; &#160; &#160;  Germany, September 8-10, 2014.</i>, 2014, vol. 1226, pp. 18–19.</div>\n" + 
				"  </div>";
		assertEquals("S. Doerfel, D. Zoller, P. Singer, T. Niebler, A. Hotho, and M. Strohmaier, “Evaluating Assumptions about Social Tagging - A Study of User Behavior\n" + 
				"&#160; &#160; &#160; &#160; &#160; &#160; &#160;  in BibSonomy,” in <i>Proceedings of the 16th LWA Workshops: KDML, IR and FGWM, Aachen,&#160; &#160; &#160; &#160; &#160; &#160; &#160;  Germany, September 8-10, 2014.</i>, 2014, vol. 1226, pp. 18–19.", CSLUtils.cleanBib(toClean));
	}
	
	private static CSLItemData createSamplePublication() {
		final CSLItemDataBuilder builder = new CSLItemDataBuilder();
		final CSLDateBuilder dateBuilder = new CSLDateBuilder();
		final CSLNameBuilder nameBuilder = new CSLNameBuilder();
		nameBuilder.family("Doe").given("John");
		dateBuilder.literal("2010");
		builder.pageFirst(100).numberOfPages("10").title("Test")
		.issued(dateBuilder.build())
		.type(CSLType.ARTICLE)
		.author(nameBuilder.build());
		
		return builder.build();
	}

	/**
	 * Finer {@code CSLItemData#equals} to identify what is different in case {@code !expected.equals(actual)}
	 */
	private static void assertCslItemDataEquals(CSLItemData expected, CSLItemData actual) {
		assertEquals(expected.getType(), actual.getType(), "Type");
		assertEquals(expected.getGenre(), actual.getGenre(), "Genre (e.g. ('dissertation')");
		assertArrayEquals(expected.getAuthor(), actual.getAuthor(), "Author");
		{
			int[][] expectedDateParts = expected.getIssued().getDateParts();
			int[][] actualDateParts = actual.getIssued().getDateParts();
			assertNotNull(expectedDateParts, "Expected date cannot be null");
			assertNotNull(actualDateParts, "Actual date cannot be null");
			assertEquals(expectedDateParts.length, actualDateParts.length, "DateParts.length");
			for (int i = 0; i < expectedDateParts.length; i++) {
				assertArrayEquals(expectedDateParts[i], actualDateParts[i], "DateParts");
			}
		}
		assertEquals(expected.getTitle(), actual.getTitle(), "Title");
		assertEquals(expected.getEdition(), actual.getEdition(), "Edition");
		assertEquals(expected.getEventPlace(), actual.getEventPlace(), "EventPlace");
		assertEquals(expected.getPublisher(), actual.getPublisher(), "Publisher");
		assertEquals(expected.getLanguage(), actual.getLanguage(), "Language");
	}

	@Test
	void shouldNotModifyFirstNames() throws PersonNameParser.PersonListParserException {
		BibTex dummyBibTex = new BibTex();
		dummyBibTex.setEntrytype(BibTexUtils.BOOK);
		dummyBibTex.setAuthor(PersonNameParser.parse("Last Name, First Name" ));

		Post<BibTex> dummyBibTexPost = new Post<>();
		dummyBibTexPost.setResource(dummyBibTex);
		dummyBibTexPost.setUser(new User("Poster"));

		assertEquals("First Name", CSLUtils.convertPost(dummyBibTexPost).getAuthor()[0].getGiven());
	}

	@Test
	void shouldNotModifyFirstNamesInPostLists() throws PersonNameParser.PersonListParserException {
		BibTex bibTex1 = new BibTex();
		bibTex1.setEntrytype(BibTexUtils.BOOK);
		bibTex1.setAuthor(PersonNameParser.parse("Last Name, First Name 1 and Last Name, First Name 2 and Last Name, First Name 3"));

		Post<BibTex> bibTexPost1 = new Post<>();
		bibTexPost1.setResource(bibTex1);
		bibTexPost1.setUser(new User("Poster"));

		BibTex bibTex2 = new BibTex();
		bibTex2.setEntrytype(BibTexUtils.BOOK);
		bibTex2.setAuthor(PersonNameParser.parse("Last Name, First Name 1 and Last Name, First Name 2 and Last Name, First Name 3"));

		Post<BibTex> bibTexPost2 = new Post<>();
		bibTexPost2.setResource(bibTex2);
		bibTexPost2.setUser(new User("Poster"));

		List<Post<BibTex>> bibItems = Arrays.asList(bibTexPost1, bibTexPost2);

		assertEquals("First Name 1", CSLUtils.convertToCslItemData(bibItems).get(0).getAuthor()[0].getGiven());
		assertEquals("First Name 2", CSLUtils.convertToCslItemData(bibItems).get(0).getAuthor()[1].getGiven());
		assertEquals("First Name 1", CSLUtils.convertToCslItemData(bibItems).get(1).getAuthor()[0].getGiven());
	}

	@Test
	void shouldConvertAManuallyCreatedBibtexBook() throws IOException, PersonNameParser.PersonListParserException {
		final CSLItemData expectedCslItemData;
		try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("de/academicpuma/opencms/core/util/book_sample01.bib")) {
			BibliographyFileReader reader = new BibliographyFileReader();
			ItemDataProvider provider = reader.readBibliographyFile(is, BibliographyFileReader.FileFormat.BIBTEX);
			expectedCslItemData = provider.retrieveItem(provider.getIds()[0]);
		}

		BibTex bibTex = new BibTex();
		bibTex.setEntrytype(BibTexUtils.BOOK);
		bibTex.setAuthor(PersonNameParser.parse("Mustermann, Max"));
		bibTex.setYear("1999");
		bibTex.setTitle("Buchtitel1");
		bibTex.setEdition("Auflage");
		bibTex.setMisc("location = {Adresse}, language = {language}");
		bibTex.setPublisher("Verlag");

		Post<BibTex> bibTexPost = new Post<>();
		bibTexPost.setResource(bibTex);
		bibTexPost.setUser(new User("Poster"));

		final CSLItemData actualCslItemData = CSLUtils.convertPost(bibTexPost);

		assertCslItemDataEquals(expectedCslItemData, actualCslItemData);
	}

	@Test
	void shouldConvertAParsedXmlBook() throws IOException {
		final CSLItemData expectedCslItemData;
		try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("de/academicpuma/opencms/core/util/book_sample01.bib")) {
			Objects.requireNonNull(is);
			BibliographyFileReader reader = new BibliographyFileReader();
			ItemDataProvider provider = reader.readBibliographyFile(is, BibliographyFileReader.FileFormat.BIBTEX);
			expectedCslItemData = provider.retrieveItem(provider.getIds()[0]);
		}

		XMLRenderer xmlRenderer = new XMLRenderer(new UrlRenderer("NOT NEEDED?"));
		xmlRenderer.init();

		final List<Post<? extends Resource>> posts;
		try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("de/academicpuma/opencms/core/util/book_sample01.xml")) {
			Objects.requireNonNull(is);
			Reader reader = new InputStreamReader(is);
			posts = xmlRenderer.parsePostList(reader, null);
		}
		@SuppressWarnings("unchecked")
		final Post<BibTex> post = (Post<BibTex>)posts.get(0);

		final CSLItemData actualCslItemData = CSLUtils.convertPost(post);

		assertCslItemDataEquals(expectedCslItemData, actualCslItemData);
	}

	@Test
	void shouldHandleBibtexLanguage() {
		final String EXPECTED_TITLE = "Ignore tittle";
		final int EXPECTED_YEAR = 1999;
		final String EXPECTED_BOOKTITTLE = "Buchtitle auf Deutsch";
		final String EXPECTED_EVENT = "Ignore event";
		final String EXPECTED_EVENT_PLACE = "Ignore event Place";
		final int EXPECTED_EDITION = 99;
		final String EXPECTED_PUBLISHER = "Ignore Publisher";
		final String EXPECTED_LANGUAGE = "german";

		BibTex bibTex = new BibTex();
		bibTex.setEntrytype(BibTexUtils.INPROCEEDINGS);
		bibTex.setYear("" + EXPECTED_YEAR);
		bibTex.setTitle(EXPECTED_TITLE);
		bibTex.setBooktitle(EXPECTED_BOOKTITTLE);
		// Language only in misc
		bibTex.setMisc(String.format("eventtitle = {%s}, venue = {%s}, language = {%s}", EXPECTED_EVENT, EXPECTED_EVENT_PLACE, EXPECTED_LANGUAGE));
		bibTex.setEdition("" + EXPECTED_EDITION);
		bibTex.setPublisher(EXPECTED_PUBLISHER);

		Post<BibTex> bibTexPost = new Post<>();
		bibTexPost.setResource(bibTex);
		bibTexPost.setUser(new User("Poster"));

		final CSLItemData expectedCslItemData = new CSLItemDataBuilder()
				.type(CSLType.PAPER_CONFERENCE)
				.issued(new CSLDateBuilder().dateParts(EXPECTED_YEAR).build())
				.title(EXPECTED_TITLE)
				.containerTitle(EXPECTED_BOOKTITTLE)
				.event(EXPECTED_EVENT)
				.eventPlace(EXPECTED_EVENT_PLACE)
				.edition(EXPECTED_EDITION)
				.publisher(EXPECTED_PUBLISHER)
				.language(EXPECTED_LANGUAGE)
				.build();

		final CSLItemData actualCslItemData = CSLUtils.convertPost(bibTexPost);

		assertCslItemDataEquals(expectedCslItemData, actualCslItemData);
	}

	@Test
	void verifyCiteprocRenderingOfPhdThesis() throws IOException, ParseException {
		// From http://medi.uni-oldenburg.de/html/DissertationBibFiles/dissertation.html
		final String DISSERTATION_IN_BIBTEX =
				"@PhdThesis{Wang:2002:DIR,\n" +
						"  author =       \"Yu-Chung Wang\",\n" +
						"  title =        \"Design and implementation of {RED-Linux}\",\n" +
						"  type =         \"Thesis (Ph.D.)\",\n" +
						"  school =       \"Electrical and Computer Engineering, University of\n" +
						"                 California, Irvine\",\n" +
						"  address =      \"Irvine, CA, USA\",\n" +
						"  year =         \"2002\",\n" +
						"  LCCN =         \"LD 791.9 .E38 2002 W36 Bar\",\n" +
						"  bibdate =      \"Sun Mar 23 07:05:13 MST 2003\",\n" +
						"  bibsource =    \"http://www.math.utah.edu/pub/tex/bib/unix.bib\",\n" +
						"  acknowledgement = ack-nhfb,\n" +
						"  keywords =     \"computer algorithms; dissertations, academic --\n" +
						"                 University of California, Irvine -- electrical and\n" +
						"                 computer engineering; Linux; operating systems\n" +
						"                 (computers); real-time data processing; scheduling --\n" +
						"                 data processing\",\n" +
						"}";

		final String EXPECTED = "Wang, Y.-C. (2002). Design and implementation of RED-Linux (Thesis (Ph.D.))." +
				" Electrical and Computer Engineering, University of California, Irvine, Irvine, CA, USA.";

		// Citeproc-java must render dissertations correctly
		BibTeXDatabase db = new BibTeXConverter().loadDatabase(new StringInputStream(DISSERTATION_IN_BIBTEX));
		final BibTeXItemDataProvider provider = new BibTeXItemDataProvider();
		provider.addDatabase(db);
		final CSLItemData expectedCslItemDataFromBibTeX = provider.retrieveItem(provider.getIds()[0]);

		Bibliography actualBibliographyCiteproc = CSL.makeAdhocBibliography("apa", "text", expectedCslItemDataFromBibTeX);
		assertEquals(1, actualBibliographyCiteproc.getEntries().length, "It should generate only one bibliographic entry");
		assertEquals(EXPECTED, actualBibliographyCiteproc.makeString().trim(), "citeproc-java must print 'Thesis (Ph.D.)'");
	}

	@Test
	void testRenderingOfPhdThesis() throws IOException, ParseException {
		// From http://medi.uni-oldenburg.de/html/DissertationBibFiles/dissertation.html
		final String DISSERTATION_IN_BIBTEX =
				"@PhdThesis{Wang:2002:DIR,\n" +
				"  author =       \"Yu-Chung Wang\",\n" +
				"  title =        \"Design and implementation of {RED-Linux}\",\n" +
				"  type =         \"Thesis (Ph.D.)\",\n" +
				"  school =       \"Electrical and Computer Engineering, University of\n" +
				"                 California, Irvine\",\n" +
				"  address =      \"Irvine, CA, USA\",\n" +
				"  year =         \"2002\",\n" +
				"  LCCN =         \"LD 791.9 .E38 2002 W36 Bar\",\n" +
				"  bibdate =      \"Sun Mar 23 07:05:13 MST 2003\",\n" +
				"  bibsource =    \"http://www.math.utah.edu/pub/tex/bib/unix.bib\",\n" +
				"  acknowledgement = ack-nhfb,\n" +
				"  keywords =     \"computer algorithms; dissertations, academic --\n" +
				"                 University of California, Irvine -- electrical and\n" +
				"                 computer engineering; Linux; operating systems\n" +
				"                 (computers); real-time data processing; scheduling --\n" +
				"                 data processing\",\n" +
				"}";

		final String DISSERTATION_IN_JSON_PUMA = "{\n" +
				"  \"post\": {\n" +
				"    \"user\": {\n" +
				"      \"name\": \"gallardo\",\n" +
				"      \"href\": \"https://puma.ub.uni-stuttgart.de/api/users/gallardo\"\n" +
				"    },\n" +
				"    \"bibtex\": {\n" +
				"      \"title\": \"Design and implementation of {RED-Linux}\",\n" +
				"      \"bibtexKey\": \"Wang:2002:DIR\",\n" +
				"      \"misc\": \"  lccn = {LD 791.9 .E38 2002 W36 Bar},\\n  bibsource = {http://www.math.utah.edu/pub/tex/bib/unix.bib},\\n  bibdate = {Sun Mar 23 07:05:13 MST 2003}\",\n" +
				"      \"entrytype\": \"phdthesis\",\n" +
				"      \"address\": \"Irvine, CA, USA\",\n" +
				"      \"author\": \"Wang, Yu-Chung\",\n" +
				"      \"school\": \"Electrical and Computer Engineering, University of California, Irvine\",\n" +
				"      \"year\": \"2002\",\n" +
				"      \"type\": \"Thesis (Ph.D.)\"\n" +
				"    }\n" +
				"  }\n" +
				"}";

		final String EXPECTED = "Wang, Y.-C. (2002). Design and implementation of RED-Linux (Thesis (Ph.D.))." +
				" Electrical and Computer Engineering, University of California, Irvine, Irvine, CA, USA.";

		// Use citeproc-java directly to build the CSLItems from BibTeX
		BibTeXDatabase db = new BibTeXConverter().loadDatabase(new StringInputStream(DISSERTATION_IN_BIBTEX));
		final BibTeXItemDataProvider provider = new BibTeXItemDataProvider();
		provider.addDatabase(db);
		final CSLItemData EXPECTED_CSL_ITEM_DATA_FROM_BIBTEX = provider.retrieveItem(provider.getIds()[0]);

		// actual test
		JSONRenderer jsonRenderer = new JSONRenderer(new UrlRenderer("NOT NEEDED?"));
		Reader reader = new InputStreamReader(new StringInputStream(DISSERTATION_IN_JSON_PUMA));
		@SuppressWarnings("unchecked") final Post<BibTex> post = (Post<BibTex>)jsonRenderer.parsePost(reader, null);
		final CSLItemData actualCslItemData = CSLUtils.convertPost(post);

		// compare csl
		assertCslItemDataEquals(EXPECTED_CSL_ITEM_DATA_FROM_BIBTEX, actualCslItemData);

		// compare text rendering
		Bibliography actualBibliography = CSL.makeAdhocBibliography("apa", "text", actualCslItemData);
		assertEquals(1, actualBibliography.getEntries().length, "It should generate only one bibliographic entry");
		assertEquals(EXPECTED, actualBibliography.makeString().trim());
	}

	@Test
	void verifyCiteprocRenderingOfDissertations() throws IOException, ParseException {
		// From http://medi.uni-oldenburg.de/html/DissertationBibFiles/dissertation.html
		final String DISSERTATION_IN_BIBTEX = "@phdthesis{\n" +
				"\n" +
				"\t\tType = {dissertation},\n" +
				"\t\t\t\tTitle = {Factors Influencing Sentence Intelligibility in Noise},\n" +
				"\t\t\t\tAuthor = {Kirsten C. Wagener},\n" +
				"\t\t\t\tSchool = {University of Oldenburg},\n" +
				"\t\t\t\tURL = {http://docserver.bis.uni-oldenburg.de/publikationen/dissertation/2003/wagfac03/wagfac03.html},\n" +
				"\t\tYear = {2003},\n" +
				"}";
		// From http://medi.uni-oldenburg.de/html/DissertationBibFiles/dissertation.html, imported in Zotero and exported to CSL/JSON
		final String DISSERTATION_IN_JSON = "\n" +
				"\t{\n" +
				"\t\t\"id\": \"http://zotero.org/users/local/xesCO2jf/items/EZXMZ8B6\",\n" +
				"\t\t\"type\": \"thesis\",\n" +
				"\t\t\"title\": \"Factors Influencing Sentence Intelligibility in Noise\",\n" +
				"\t\t\"publisher\": \"University of Oldenburg\",\n" +
				"\t\t\"genre\": \"dissertation\",\n" +
				"\t\t\"URL\": \"http://docserver.bis.uni-oldenburg.de/publikationen/dissertation/2003/wagfac03/wagfac03.html\",\n" +
				"\t\t\"author\": [\n" +
				"\t\t\t{\n" +
				"\t\t\t\t\"family\": \"Wagener\",\n" +
				"\t\t\t\t\"given\": \"Kirsten C.\"\n" +
				"\t\t\t}\n" +
				"\t\t],\n" +
				"\t\t\"issued\": {\n" +
				"\t\t\t\"date-parts\": [\n" +
				"\t\t\t\t[\n" +
				"\t\t\t\t\t\"2003\"\n" +
				"\t\t\t\t]\n" +
				"\t\t\t]\n" +
				"\t\t}\n" +
				"\t}\n" +
				"";

		final String EXPECTED = "Wagener, K. C. (2003). Factors Influencing Sentence Intelligibility in Noise (Dissertation, University of Oldenburg)." +
				" Retrieved from http://docserver.bis.uni-oldenburg.de/publikationen/dissertation/2003/wagfac03/wagfac03.html";

		// Citeproc-java must render dissertations correctly when read from BIBTEX
		BibTeXDatabase db = new BibTeXConverter().loadDatabase(new StringInputStream(DISSERTATION_IN_BIBTEX));
		final BibTeXItemDataProvider provider = new BibTeXItemDataProvider();
		provider.addDatabase(db);
		final CSLItemData expectedCslItemDataFromBibTeX = provider.retrieveItem(provider.getIds()[0]);

		final Bibliography actualBibliographyCiteproc = CSL.makeAdhocBibliography("apa", "text", expectedCslItemDataFromBibTeX);
		assertEquals(1, actualBibliographyCiteproc.getEntries().length, "It should generate only one bibliographic entry");
		assertEquals(EXPECTED, actualBibliographyCiteproc.makeString().trim(), "citeproc-java must print 'Dissertation'");

		// Citeproc-java must render dissertations correctly when read from from JSON
		final JsonLexer jsonLexer = new JsonLexer(new StringReader(DISSERTATION_IN_JSON));
		final JsonParser jsonParser = new JsonParser(jsonLexer);
		final Map<String, Object> dissertationObjectMap = jsonParser.parseObject();
		final CSLItemData expectedCslItemDataFromJSON = CSLItemData.fromJson(dissertationObjectMap);

		assertCslItemDataEquals(expectedCslItemDataFromBibTeX, expectedCslItemDataFromJSON);

		// Do not use makeAdhocBibliography: instead, create the CSL object manually
		final CSLItemData[] expectedCslItemDataArray = new CSLItemData[1];
		expectedCslItemDataArray[0] = expectedCslItemDataFromJSON;
		final ListItemDataProvider expectedListItemDataProvider = new ListItemDataProvider(expectedCslItemDataArray);
		final CSL expectedCiteproc = new CSL(expectedListItemDataProvider, new DefaultAbbreviationProvider(), "apa");
		expectedCiteproc.registerCitationItems(expectedListItemDataProvider.getIds());
		expectedCiteproc.setOutputFormat("text");
		final String ACTUAL = expectedCiteproc.makeBibliography().makeString();
		assertEquals(EXPECTED, ACTUAL.trim(), "citeproc-java must print 'Dissertation'");
	}

	@Test
	void testRenderingOfDissertations() throws IOException, ParseException {
		// From http://medi.uni-oldenburg.de/html/DissertationBibFiles/dissertation.html
		final String DISSERTATION_IN_BIBTEX = "@phdthesis{\n" +
				"\n" +
				"\t\tType = {dissertation},\n" +
				"\t\t\t\tTitle = {Factors Influencing Sentence Intelligibility in Noise},\n" +
				"\t\t\t\tAuthor = {Kirsten C. Wagener},\n" +
				"\t\t\t\tSchool = {University of Oldenburg},\n" +
				"\t\t\t\tURL = {http://docserver.bis.uni-oldenburg.de/publikationen/dissertation/2003/wagfac03/wagfac03.html},\n" +
				"\t\tYear = {2003},\n" +
				"}";

		final String DISSERTATION_IN_JSON_PUMA = "{\n" +
				"  \"post\": {\n" +
				"    \"user\": {\n" +
				"      \"name\": \"testuser\",\n" +
				"      \"href\": \"https://puma.ub.uni-stuttgart.de/api/users/testuser\"\n" +
				"    },\n" +
				"    \"bibtex\": {\n" +
				"      \"title\": \"Factors {Influencing} {Sentence} {Intelligibility} in {Noise}\",\n" +
				"      \"bibtexKey\": \"wagener_factors_2003\",\n" +
				"      \"entrytype\": \"phdthesis\",\n" +
				"      \"author\": \"Wagener, Kirsten C.\",\n" +
				"      \"school\": \"University of Oldenburg\",\n" +
				"      \"year\": \"2003\",\n" +
				"      \"type\": \"dissertation\",\n" +
				"      \"url\": \"http://docserver.bis.uni-oldenburg.de/publikationen/dissertation/2003/wagfac03/wagfac03.html\"\n" +
				"    }\n" +
				"  }\n" +
				"}";

		final String EXPECTED = "Wagener, K. C. (2003). Factors Influencing Sentence Intelligibility in Noise (Dissertation, University of Oldenburg)." +
				" Retrieved from http://docserver.bis.uni-oldenburg.de/publikationen/dissertation/2003/wagfac03/wagfac03.html";

		// Use citeproc-java directly to build the CSLItems from BibTeX
		BibTeXDatabase db = new BibTeXConverter().loadDatabase(new StringInputStream(DISSERTATION_IN_BIBTEX));
		final BibTeXItemDataProvider provider = new BibTeXItemDataProvider();
		provider.addDatabase(db);
		final CSLItemData EXPECTED_CSL_ITEM_DATA_FROM_BIBTEX = provider.retrieveItem(provider.getIds()[0]);

		// actual test
		JSONRenderer jsonRenderer = new JSONRenderer(new UrlRenderer("NOT NEEDED?"));
		Reader reader = new InputStreamReader(new StringInputStream(DISSERTATION_IN_JSON_PUMA));
		@SuppressWarnings("unchecked") final Post<BibTex> post = (Post<BibTex>)jsonRenderer.parsePost(reader, null);
		final CSLItemData actualCslItemData = CSLUtils.convertPost(post);

		// compare csl
		assertCslItemDataEquals(EXPECTED_CSL_ITEM_DATA_FROM_BIBTEX, actualCslItemData);

		// compare text rendering
		Bibliography actualBibliography = CSL.makeAdhocBibliography("apa", "text", actualCslItemData);
		assertEquals(1, actualBibliography.getEntries().length, "It should generate only one bibliographic entry");
		assertEquals(EXPECTED, actualBibliography.makeString().trim());

	}
}
