/*
 * This library is part of OpenCms -
 * the Open Source Content Management System
 *
 * Copyright (c) Alkacon Software GmbH & Co. KG (http://www.alkacon.com)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * For further information about Alkacon Software, please see the
 * company website: http://www.alkacon.com
 *
 * For further information about OpenCms, please see the
 * project website: http://www.opencms.org
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

package de.academicpuma.opencms.core.util;

import org.apache.commons.collections.Transformer;
import org.opencms.file.CmsObject;
import org.opencms.jsp.util.CmsJspContentAccessValueWrapper;
import org.opencms.util.CmsCollectionsGenericWrapper;
import org.opencms.xml.I_CmsXmlDocument;
import org.opencms.xml.types.I_CmsXmlContentValue;

import java.util.Locale;
import java.util.Map;

/**
 * Mimics {@code CmsJspContentAccessValueWrapper}, providing a factory method for
 * {@code Map<String, CmsJspContentAccessValueWrapper>}s, thus allowing access to the individual elements of an XML
 * document similar as when using the tag {@code cms:contentload}.<p>
 * <p>
 * This is a convenient factory method to simulate how jsps access the OpenCms content using
 * {@code CmsJspContentAccessValueWrapper.createWrapper} and {@code CmsJspContentAccessValueWrapper.getValue}.
 * Unfortunately, {@code CmsJspContentAccessValueWrapper.createWrapper} requires a fully initialized {@code CmsObject},
 * rendering the effort of using that factory method for unit tests absurdly high.<p>
 *
 * @see org.opencms.jsp.CmsJspTagContentAccess
 * @see org.opencms.jsp.util.CmsJspContentAccessValueWrapper
 * @see org.opencms.jsp.util.CmsJspContentAccessBean
 */
public class CmsJspContentAccessValueWrapperFactory {
	/**
	 * Returns a lazy initialized Map that provides values from the XML content in the current locale.<p>
	 * <p>
	 * The provided Map key is assumed to be a String that represents the xpath to the value.<p>
	 * <p>
	 * Usage example on a JSP with the JSTL:<pre>
	 * &lt;cms:contentload ... &gt;
	 *     &lt;cms:contentaccess var="content" /&gt;
	 *     The Title: ${content.value['Title']}
	 * &lt;/cms:contentload&gt;</pre>
	 *
	 * @return a lazy initialized Map that provides values from the XML content in the current locale
	 */
	public static Map<String, CmsJspContentAccessValueWrapper> getValue(I_CmsXmlDocument content, Locale locale) {
		return CmsCollectionsGenericWrapper.createLazyMap(new CmsValueTransformer(content, locale));
	}

	/**
	 * Provides a Map which lets the user access a value in a XML content,
	 * the input is assumed to be a String that represents an xpath in the XML content.<p>
	 */
	private static class CmsValueTransformer implements Transformer {

		/**
		 * The XML content to access.
		 */
		private I_CmsXmlDocument content;

		/**
		 * The selected locale.
		 */
		private Locale locale;

		/**
		 * @param xmlDocument with the raw data
		 * @param locale      selected locale
		 */
		CmsValueTransformer(I_CmsXmlDocument xmlDocument, Locale locale) {

			this.content = xmlDocument;
			this.locale = locale;
		}

		/**
		 * Creates a wrapper for the value associated with the key {@code input}
		 *
		 * @param input key name (will be read using {@link String#valueOf(Object)} )
		 * @return {@code CmsJspContentAccessValueWrapper} wrapping the value of interest
		 */
		@Override
		public Object transform(Object input) {

			I_CmsXmlContentValue value = content.getValue(String.valueOf(input), locale);

			return CmsJspContentAccessValueWrapper.createWrapper(new CmsObject(null, null), value, content, (String) input, locale);
		}

	}

}