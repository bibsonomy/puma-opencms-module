package de.academicpuma.opencms.core.util;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.User;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.model.util.PersonNameParser;

import java.util.List;

public class Posts {
	/**
	 * Helper method to create BibTex Posts
	 *
	 * @param year set the year of the bibtex publication of this post
	 * @return post with dumy fields: {@code
	 * "post":[{"user":{"name":"testuser1"},
	 * "bibtex":{ "title":"test", "author":"Doe, John"}
	 * }]
	 * }
	 */
	public static Post<BibTex> generatePublicationPost(int year, List<PersonName> authors) {
		final Post<BibTex> post = new Post<>();
		final BibTex publication = new BibTex();
		publication.setEntrytype(BibTexUtils.ARTICLE);
		publication.setTitle("test");
		publication.setAuthor(authors);
		publication.setYear(String.valueOf(year));
		publication.recalculateHashes();

		post.setUser(new User("testuser1"));
		post.setResource(publication);
		return post;
	}

	public static Post<BibTex> generatePublicationPost(int year) {
		try {
			return generatePublicationPost(year, PersonNameParser.parse("Doe, John"));
		} catch (PersonNameParser.PersonListParserException e) {
			throw new AssertionError("Error creating dummy person: " + e.getMessage(), e);
		}
	}

	public static Post<BibTex> generatePublicationPost(String year, String month, String day, String intraHash) {
		Post<BibTex> post = new Post<>();
		final BibTex bibTex2017Nov11 = new BibTex();
		bibTex2017Nov11.setYear(year);
		bibTex2017Nov11.setMonth(month);
		bibTex2017Nov11.setDay(day);
		bibTex2017Nov11.setIntraHash(intraHash);

		post.setResource(bibTex2017Nov11);
		return post;
	}
}
