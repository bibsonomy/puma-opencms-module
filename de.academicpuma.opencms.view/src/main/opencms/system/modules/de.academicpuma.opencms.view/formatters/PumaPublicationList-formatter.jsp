<%@ page import="org.opencms.main.CmsException" %>
<%@ page buffer="none" session="false" trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>

<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="de.academicpuma.opencms.view.messages">

<cms:formatter var="content">
	<div class="margin-bottom-30">

		<div class="puma-headline">
			<c:if test="${!content.value.Title.isEmptyOrWhitespaceOnly}">
				<h3 ${content.rdfa.Title}>${content.value.Title}</h3>
			</c:if>
		</div>

		<div class="puma-bib">
			<c:choose>
				<c:when test="${cms.element.inMemoryOnly}">
					<div class="dummy dummy-faqliste">
						<div class="dummy-content">
							<ol>
								<li>
									<div class="publication">
										<div class="csl-entry">Herbst-Damm, K. L., &amp; Kulik, J. A. (2005). 01 -
											Volunteer support, marital status, and the survival times of terminally ill
											patients. <i>Health Psychology</i>, <i>24</i>, 225–229. <a
													href="https://doi.org/10.1037/0278-6133.24.2.225">https://doi.org/10.1037/0278-6133.24.2.225</a>
										</div>
									</div>
								</li>
								<li>
									<div class="publication">
										<div class="csl-entry">Gilbert, D. G., McClernon, J. F., Rabinovich, N. E.,
											Sugai, C., Plath, L. C., Asgaard, G., … Botros, N. (2004). 02 - Effects of
											quitting smoking on EEG activation and attention last for more than 31 days
											and are more severe with stress, dependence, DRD2 A1 allele, and depressive
											traits. <i>Nicotine &amp; Tobacco Research</i>, <i>6</i>, 249–267. <a
													href="https://doi.org/10.1080/14622200410001676305">https://doi.org/10.1080/14622200410001676305</a>
										</div>
									</div>
								</li>
								<li>
									<div class="publication">
										<div class="csl-entry">Sillick, T. J., &amp; Schutte, N. S. (2006). 03a -
											Emotional intelligence and self-esteem mediate between perceived early
											parental love and adult happiness. <i>E-Journal of Applied Psychology</i>,
											<i>2</i>(2), 38–48. Retrieved from <a
													href="http://ojs.lib.swin.edu.au/index.php/ejap">http://ojs.lib.swin.edu.au/index.php/ejap</a>
										</div>
									</div>
								</li>
							</ol>
						</div>
					</div>
				</c:when>
				<c:when test="${cms.edited and (variations == 'accordion' or variations == 'allinone')}">
					<div class="alert alert-danger">
						<fmt:message key="label.BibSonomyType.setting.hinweis"/>
					</div>
					<div style="text-align:center;padding:20px;">
						<a href="<cms:link>${cms.requestContext.uri}</cms:link>" class="btn btn-warning" type="button"
						   title=""><fmt:message key="label.BibSonomyType.button.neuLaden"/></a>
					</div>
				</c:when>

				<c:otherwise>
					${cms.enableReload}

					<c:catch var="exception">
						<jsp:useBean id="cslRenderer" class="de.academicpuma.opencms.core.JspCSLRenderBean"/>
						<c:set var="renderedBibliography">
							<%
								cslRenderer.init(pageContext, request, response);
							%>
							<div>${cslRenderer.renderCSL(content.value)}</div>
						</c:set>
					</c:catch>

					<c:if test="${exception != null}">
						<c:choose>
							<c:when test="${not cms.isOnlineProject }">
								<div class="alert alert-danger">
									<strong><fmt:message key="msg.pumaPublicationList.error.renderException"/></strong>:<%
										Exception e = (Exception) pageContext.getAttribute("exception");
										if (e instanceof CmsException) {
											out.print(((CmsException) e).getLocalizedMessage(request.getLocale()));
										} else {
											out.print(e.getLocalizedMessage());
										}
									%><br/>
									<pre><code><% e.printStackTrace(new java.io.PrintWriter(out)); %></code></pre>
								</div>
							</c:when>

							<c:otherwise>
								<div class="alert alert-warning">
									<strong><fmt:message key="msg.pumaPublicationList.error.renderException"/></strong>
								</div>
							</c:otherwise>
						</c:choose>
					</c:if>

					<c:if test="${exception == null}">
						<c:out value="${renderedBibliography}" escapeXml="false" />
					</c:if>

				</c:otherwise>
		</c:choose>
		</div>

	</div>
</cms:formatter>

</cms:bundle>
