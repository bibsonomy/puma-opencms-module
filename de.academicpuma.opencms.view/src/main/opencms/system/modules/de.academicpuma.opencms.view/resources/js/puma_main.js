function toggle(id, event) {
	var element = document.getElementById(id);
	var classNames = element.className;
	var newClass = removeClass(classNames, 'hidden');
	if (newClass == classNames) {
		newClass = classNames + ' hidden';
	}
	
	element.className = newClass;
	
	event.preventDefault();
}

function removeClass(classNames, classToRemove) {
	var classes = classNames.split(' ');
	var newClass = [];
	for (var i = 0; i < classes.length; i++) {
		var className = classes[i];
		if (className != classToRemove) {
			newClass.push(className);
		}
	}
	
	return newClass.join(' ');
}