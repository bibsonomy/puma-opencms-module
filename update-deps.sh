#!/usr/bin/env bash

MODULE=de.academicpuma.opencms.core

# pre-download required artifacts and ignore
mvn -f $MODULE/pom.xml help:evaluate -Dexpression=project.artifactId 

# Read pom artifact
project_artifactId=$(mvn -f $MODULE/pom.xml help:evaluate -Dexpression=project.artifactId | egrep -v "^\[")

# Standard library path in opencms module
PATH_IN_ASSEMBLY=system/modules/${project_artifactId}/lib

BEGING_MARK="		<!-- Begin of dependencies section -->"
END_MARK="		<!-- End of dependencies section -->"
MANIFEST_FILE="$MODULE/src/main/opencms/manifest.xml"
echo "Updating dependencies in the manifest file \'$MANIFEST_FILE\'"
echo "The lines between the two marks \'$BEGING_MARK\' and \'$END_MARK\' will be rewritten"
for dependency in $MODULE/target/dependency/*.jar
do
	dependencyFileName=${dependency#${MODULE}/target/dependency/}
	echo "$dependencyFileName will be included in manifest under $PATH_IN_ASSEMBLY/$dependencyFileName"
	# Unfortunately, sed expect new lines explicitly as \n
	dependencyListXml="$dependencyListXml		<file>\n			<source>$PATH_IN_ASSEMBLY/$dependencyFileName</source>\n			<destination>$PATH_IN_ASSEMBLY/$dependencyFileName</destination>\n			<type>binary</type>\n			<flags>0</flags>\n			<properties/>\n			<accesscontrol/>\n		</file>\n"
done
sed -i "/$BEGING_MARK/,/$END_MARK/c\\$BEGING_MARK\n$dependencyListXml$END_MARK" $MANIFEST_FILE
